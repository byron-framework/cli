export interface YAMLAttribute {
  name: string;
  type: string;
}

export interface YAMLObjDeclaration {
  name: string;
  attributes: YAMLAttribute[];
}

// tslint:disable-next-line: no-empty-interface
export interface YAMLInput extends YAMLObjDeclaration {}

// tslint:disable-next-line: no-empty-interface
export interface YAMLEntity extends YAMLObjDeclaration {}

// tslint:disable-next-line: no-empty-interface
export interface YAMLParameter extends YAMLAttribute {}

export interface YAMLCommand {
  name: string;
  type: string;
  returnType: string;
  parameters?: YAMLAttribute[];
}

export interface YAMLHandler {
  name: string;
  event: string;
}

interface YAMLAPIConfig {
  port: number;
}

export interface YAMLConfig {
  api: YAMLAPIConfig;
}

interface YAMLContent {
  types: YAMLEntity[];
  inputs: YAMLInput[];
  commands: YAMLCommand[];
  handlers: YAMLHandler[];
}

export interface YAMLComponent {
  name: string;
  namespace: string;
  content: YAMLContent;
  config?: YAMLConfig;
}
