export interface Commands {
  [manager: string]: () => void;
}

export interface CommandItem {
  type: string;
  name: string;
  description: string;
}

export interface HelpSubMenu {
  name: string;
  items: CommandItem[];
}

export interface HelpMenuView {
  usage: string;
  summary: string;
  lists: HelpSubMenu[];
  formatItem(): string;
}

export const HELP_SPACING: number = 12;
