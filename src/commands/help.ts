import { readFileSync } from 'fs-extra';
import { render } from 'mustache';
import { resolve } from 'path';

import { HELP_SPACING, HelpMenuView } from 'src/typings/commands/cli';

const TEMPLATE_PATH: string = resolve(__dirname, '..', 'templates', 'HelpMenu');

export const view: HelpMenuView = {
  usage: 'byron MANAGER COMMAND [OPTIONS]',
  summary: 'A cloud-native reactive microservices framework',
  lists: [{
    name: 'Options',
    items: [
      {
        type: 'option',
        name: 'help',
        description: 'Print this menu and exit',
      },
      {
        type: 'option',
        name: 'version',
        description: 'Print version information and exit',
      },
    ],
  }],
  formatItem(): string {
    const { name, description, type }: any = this;

    switch (type) {
      case 'option':
        return `--${name}${' '.repeat(HELP_SPACING - 2 - name.length)}${description}`;
      default:
        return 'help';
    }
  },

};

export function globalHelpConstructor(): string {
  const templatePath: string =  resolve(TEMPLATE_PATH, 'helpMenu.template.txt');
  const listTemplatePath: string =  resolve(TEMPLATE_PATH, 'listHelpMenu.template.txt');

  const template: string =
    readFileSync(templatePath)
    .toString();

  const listTemplate: string =
    readFileSync(listTemplatePath)
    .toString();

  return render(template, view, {
    list: listTemplate,
  });
}
