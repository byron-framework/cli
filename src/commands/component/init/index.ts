import { existsSync, mkdirSync, writeFileSync } from 'fs-extra';
import { safeDump } from 'js-yaml';
import { resolve } from 'path';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

import { help } from 'src/commands/component/init/help';

// [  0,    1,        2,      3,    4,  5,    6,    7     ] ==> 8
// [ node, byron, component, init, -n, NAME, -N namespace ]
export function init(): void {
  const { argv }: NodeJS.Process = process;

  if (argv[4] === '--help') {
    help();
    return;
  }

  if (argv.length !== 8) {
    throw new ArgumentMissingError('Missing name or namespace in component init');
  }

  const nameIndex: number = argv.findIndex((arg: string) => {
    return arg === '-n' || arg === '--name';
  });

  const namespaceIndex: number = argv.findIndex((arg: string) => {
    return arg === '-N' || arg === '--namespace';
  });

  if (nameIndex < 0 || namespaceIndex < 0) {
    throw new ArgumentMissingError('Missing name or namespace in component init');
  }

  const name: string = argv[nameIndex + 1];
  const namespace: string = argv[namespaceIndex + 1];

  const path: string = resolve(process.cwd(), name);

  if (existsSync(path)) {
    throw new FileSystemError();
  }

  const directories: string[][] = [
    ['api'],
    ['api', 'commands'],
    ['hooks'],
    ['sink'],
    ['sink', 'handlers'],
  ];

  mkdirSync(path);
  directories.forEach((dir: string[]): void => {
    const finalPath: string = resolve(path, ...dir);
    mkdirSync(finalPath);
  });

  writeFileSync(
    resolve(path, 'schema.yml'),
    safeDump({ name, namespace }),
  );
}
