import { safeLoad } from 'js-yaml';

import FSWrapper from 'src/FSWrapper';
import CompExec, { CompExecSignature } from 'src/utils/compExec';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';
import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import {
  YAMLComponent,
} from 'src/typings/interpreters/YAML/interpreter';

const helpMenu: string = 'Component Init Help Menu';

export function up(
  fsw: any = FSWrapper.instance,
  compExec: CompExecSignature = CompExec,
): void {
  const { argv }: NodeJS.Process = process;

  if (argv.length === 4) {
    throw new ArgumentMissingError();
  }

  if (argv[4] === '--help') {
    console.log(helpMenu);
    return;
  }

  const path: string = argv[4];
  const foundRootDir: boolean = fsw.checkRootDir(path);

  if (!foundRootDir) {
    throw new FileSystemError(`Path not found`);
  }

  const foundSchema: boolean = fsw.checkSchema('schema.yml');

  if (!foundSchema) {
    throw new FileSystemError(`Schema not found`);
  }

  const schema: string = fsw.readSchema();
  const parsedSchema: YAMLComponent = safeLoad(schema);

  if (!parsedSchema.name || !parsedSchema.namespace) {
    throw new SchemaSyntaxError();
  }

  const { name, namespace }: YAMLComponent = parsedSchema;
  compExec(name, namespace, 'up');
}
