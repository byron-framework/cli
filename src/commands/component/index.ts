import UnknownCommandError from 'src/errors/UnknownCommandError';

import { help } from 'src/commands/component/help';

import { build } from 'src/commands/component/build';
import { down } from 'src/commands/component/down';
import { init } from 'src/commands/component/init';
import { up } from 'src/commands/component/up';

const commands: any = {
  build,
  down,
  init,
  up,
};

export function component(): void {
  const { argv }: NodeJS.Process = process;

  if (argv.length === 3 || argv[3] === '--help') {
    help();
    return;
  }

  const commandNames: string[] = Object.keys(commands);
  if (commandNames.includes(argv[3])) {
    commands[argv[3]]();
    return;
  } else {
    throw new UnknownCommandError();
  }
}
