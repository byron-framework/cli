import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

import FSWrapper from 'src/FSWrapper';

import { interpret } from 'src/interpreters/YAML/interpreter';

import Component from 'src/component/Component';

import DockerComposeGenerator from 'src/generators/DockerComposeGenerator';
import GraphQLGenerator from 'src/generators/GraphQLGenerator';
import HandlerGenerator from 'src/generators/HandlerGenerator';
import HooksGenerator from 'src/generators/HooksGenerator';
import MongoModelGenerator from 'src/generators/MongoModelGenerator';
import Validator from 'src/generators/Validator';

// [   0 ,   1  ,     2    ,   3  ,   4  ] ==> 5
// [ node, byron, component, build, PATH ]
export function build(FSW: any = FSWrapper): void {
  const { argv }: NodeJS.Process = process;

  if (argv.length === 4) {
    throw new ArgumentMissingError();
  }

  if (argv[4] === '--help') {
    console.log(`Component Build Help Menu`);
    return;
  }

  const fsw: any = FSW.instance;

  const name: string = argv[4];
  const foundRootDir: boolean = fsw.checkRootDir(name);

  if (!foundRootDir) {
    throw new FileSystemError();
  }

  const ast: Component = interpret(FSW);

  ast.accept(new Validator());

  if (fsw.checkBuildDir(ast.namespace, ast.name)) {
    fsw.removeBuildDir();
  }

  fsw.makeBuildDirTree();

  ast.accept(new DockerComposeGenerator(fsw));
  ast.accept(new GraphQLGenerator(fsw));
  ast.accept(new HandlerGenerator(fsw));
  ast.accept(new HooksGenerator(fsw));
  ast.accept(new MongoModelGenerator(fsw));

  console.log(`Component ${ast.namespace}/${ast.name} has been built`);
}
