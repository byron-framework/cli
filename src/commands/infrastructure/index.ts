import UnknownCommandError from 'src/errors/UnknownCommandError';

import { help } from 'src/commands/infrastructure/help';

import { down } from 'src/commands/infrastructure/down';
import { up } from 'src/commands/infrastructure/up';

const commands: any = {
  up,
  down,
};

export function infrastructure(): void {
  const { argv }: NodeJS.Process = process;

  if (argv.length === 3 || argv[3] === '--help') {
    help();
    return;
  }

  const commandNames: string[] = Object.keys(commands);
  if (commandNames.includes(argv[3])) {
    commands[argv[3]]();
    return;
  } else {
    throw new UnknownCommandError();
  }
}
