import FSWrapper from 'src/FSWrapper';
import CompExec, { CompExecSignature } from 'src/utils/compExec';

const helpMenu: string = 'Infrastructure Down Help Menu';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

export function down(
  fsW: any = FSWrapper,
  compExec: CompExecSignature = CompExec,
): void {
  const fsWrapper: any = fsW.instance;
  const { argv }: NodeJS.Process = process;

  if (argv.length === 4) {
    throw new ArgumentMissingError('Missing namespace');
  }

  if (argv[4] === '--help') {
    console.log(helpMenu);
    return;
  }

  const namespaceIndex: number = argv.findIndex((arg: string) => {
    return arg === '-N' || arg === '--namespace';
  });

  if (namespaceIndex < 0) {
    throw new ArgumentMissingError('Missing namespace');
  }

  const namespace: string = argv[namespaceIndex + 1];
  const name: string = 'Broker';

  if (!fsWrapper.checkBuildDir(namespace, name)) {
    throw new FileSystemError('Broker is not up!');
  }

  compExec(name, namespace, 'down');
}
