import UnknownCommandError from 'src/errors/UnknownCommandError';

import { Commands } from 'src/typings/commands/cli';

import { globalHelpConstructor } from 'src/commands/help';

import { component } from 'src/commands/component';
import { infrastructure } from 'src/commands/infrastructure';

const commands: Commands = {
  component,
  infrastructure,
};

export function cli(
  globalHelp: () => string = globalHelpConstructor,
): void {
  const { argv }: NodeJS.Process = process;

  if (argv.length === 2 || argv[2] === '--help') {
    console.log(globalHelp());
    return;
  }

  const manager: string = argv[2];

  const knownCommands: string[] = Object.keys(commands);

  if (!knownCommands.includes(manager)) {
    throw new UnknownCommandError();
  }

  commands[manager]();

  return;
}

export default cli;
