import {
  copyFileSync,
  copySync,
  existsSync,
  mkdirpSync,
  readFileSync,
  removeSync,
  writeFileSync,
} from 'fs-extra';

import { resolve } from 'path';

class FSWrapper {
  private static _inst: FSWrapper;
  private rootDirPath: string = '';
  private schemaPath: string = '';
  private buildDirPath: string = '';

  private constructor() {}

  public static get instance(): FSWrapper {
    if (!FSWrapper._inst) {
      FSWrapper._inst = new FSWrapper();
    }

    return FSWrapper._inst;
  }

  public checkRootDir(name: string): boolean {
    const path: string = resolve(process.cwd(), name);
    this.rootDirPath = path;
    return existsSync(path);
  }

  public checkSchema(filename: string): boolean {
    const dirPath: string = this.rootDirPath;
    const filePath: string = resolve(dirPath, filename);

    if (existsSync(filePath)) {
      this.schemaPath = filePath;
      return true;
    }

    return false;
  }

  public checkBuildDir(namespace: string, name: string): boolean {
    const path: string = resolve('/', 'tmp', 'byron', namespace, name);
    this.buildDirPath = path;
    return existsSync(path);
  }

  public removeBuildDir(): void {
    removeSync(this.buildDirPath);
  }

  public readSchema(): string {
    const path: string = this.schemaPath;
    return readFileSync(path)
      .toString();
  }

  public readTemplate(templatePath: string[]): string {
    const path: string =
      resolve(__dirname, '..', 'templates', ...templatePath);
    return readFileSync(path)
      .toString();
  }

  public writeGraphQLSchema(content: string): void {
    const path: string =
      resolve(this.buildDirPath, 'api', 'src', 'graphql', 'schema.graphql');

    writeFileSync(path, content);
  }

  public writeMongoModel(name: string, content: string): void {
    const pathAPI: string =
      resolve(this.buildDirPath, 'api', 'src', 'db', 'models', `${name}.ts`);

    const pathSink: string =
      resolve(this.buildDirPath, 'sink', 'src', 'db', 'models', `${name}.ts`);

    writeFileSync(pathAPI, content);
    writeFileSync(pathSink, content);
  }

  public writeHandler(name: string, content: string): void {
    const path: string =
      resolve(this.buildDirPath, 'sink', 'src', 'handlers', `${name}.ts`);

    writeFileSync(path, content);
  }

  public writeDockerCompose(yaml: string): void {
    const path: string =
      resolve(this.buildDirPath, 'docker-compose.yaml');

    writeFileSync(path, yaml);
  }

  public copyHandler(name: string): void {
    const pathSrc: string =
      resolve(this.rootDirPath, 'sink', 'handlers', `${name}.ts`);

    const pathDest: string =
      resolve(this.buildDirPath, 'sink', 'src', 'handlers', `dev${name}.ts`);

    copyFileSync(pathSrc, pathDest);
  }

  public copyCommandHook(name: string): void {
    this.copyHook(name, 'api');
  }

  public copyHandlerHook(name: string): void {
    this.copyHook(name, 'sink');
  }

  public writeCommandHooksAggregate(agg: string): void {
    this.writeHookAggregate(agg, 'api');
  }

  public writeHandlerHooksAggregate(agg: string): void {
    this.writeHookAggregate(agg, 'sink');
  }

  public makeBuildDirTree(): void {
    mkdirpSync(this.buildDirPath);

    const rawPath: string = resolve(__dirname, '..', 'raw');
    copySync(rawPath, this.buildDirPath);

    const commandsSrcPath: string = resolve(this.rootDirPath, 'api', 'commands');
    const commandsDestPath: string = resolve(this.buildDirPath, 'api', 'src', 'commands');
    copySync(commandsSrcPath, commandsDestPath);

    const apiSrc: string = resolve(this.buildDirPath, 'api', 'src');
    const apiGraphql: string = resolve(apiSrc, 'graphql');
    mkdirpSync(apiGraphql);

    const sinkSrc: string = resolve(this.buildDirPath, 'sink', 'src');
    const sinkHandlers: string = resolve(sinkSrc, 'handlers');
    mkdirpSync(sinkHandlers);
  }

  public makeBuildBrokerDir(namespace: string, name: string): void {
    const path: string = resolve('/', 'tmp', 'byron', namespace, name);
    this.buildDirPath = path;
    mkdirpSync(path);
  }

  private copyHook(name: string, context: string): void {
    const filename: string = name + '.ts';
    const srcPath: string = resolve(this.rootDirPath, 'hooks', filename);
    const destPath: string = resolve(this.buildDirPath, context, 'src', 'hooks', filename);

    copyFileSync(srcPath, destPath);
  }

  private writeHookAggregate(agg: string, context: string): void {
    const destPath: string = resolve(this.buildDirPath, context, 'src', 'hooks', 'filter.json');
    writeFileSync(destPath, agg);
  }

}

export default FSWrapper;
