class UnknownCommandError extends Error {
  constructor(args: any = '') {
    super(args);
  }
}

export default UnknownCommandError;
