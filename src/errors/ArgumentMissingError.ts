class ArgumentMissingError extends Error {
  constructor(args: any = ``) {
    super(args);
  }
}

export default ArgumentMissingError;
