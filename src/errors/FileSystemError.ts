class FileSystemError extends Error {
  constructor(args: any = ``) {
    super(args);
  }
}

export default FileSystemError;
