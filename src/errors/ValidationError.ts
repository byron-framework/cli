class ValidationError extends Error {
  constructor(args: any = ``) {
    super(args);
  }
}

export default ValidationError;
