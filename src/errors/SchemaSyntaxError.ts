class SchemaSyntaxError extends Error {
  constructor(args: any = ``) {
    super(args);
  }
}

export default SchemaSyntaxError;
