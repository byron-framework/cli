import Field from 'src/component/Field';

import Generator from 'src/generators/Generator';

class Parameter extends Field {
  public accept(generator: Generator): void {
    generator.visitParameter(this);
  }
}

export default Parameter;
