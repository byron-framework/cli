import Leaf from 'src/component/Leaf';

import Generator from 'src/generators/Generator';

class Handler extends Leaf {
  public readonly name: string;
  public readonly event: string;

  public constructor(name: string, event: string) {
    super();

    this.name = name;
    this.event = event;
  }

  public accept(gen: Generator): void {
    gen.visitHandler(this);
  }
}

export default Handler;
