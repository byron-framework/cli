import Type from 'src/component/Type';

import Command from 'src/component/Command';
import Entity from 'src/component/Entity';
import Input from 'src/component/Input';
import Generator from 'src/generators/Generator';

type Parent = Command | Entity | Input;

class Field extends Type {
  public readonly name: string;

  constructor(name: string, originalType: string, parent: Parent) {
    super(originalType, parent);

    this.name = name;
  }

  public accept(generator: Generator): void {
    generator.visitField(this);
  }
}

export default Field;
