import InnerNode from 'src/component/InnerNode';

import Node from 'src/component/Node';
import Generator from 'src/generators/Generator';

class Command extends InnerNode {
  public readonly name: string;
  public readonly type: string;

  constructor(name: string, type: string) {
    super();

    this.name = name;
    this.type =
      type.slice(0, 1)
        .toUpperCase() +
      type.slice(1)
        .toLowerCase();
  }

  public accept(generator: Generator): void {
    generator.visitCommand(this);
    this.children.forEach((child: Node) => {
      child.accept(generator);
    });
  }
}

export default Command;
