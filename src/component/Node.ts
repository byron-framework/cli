import Generator from 'src/generators/Generator';

abstract class Node {
  public abstract accept(generator: Generator): void;
}

export default Node;
