import Type from 'src/component/Type';
import Generator from 'src/generators/Generator';

class ReturnType extends Type {
  public accept(generator: Generator): void {
    generator.visitReturnType(this);
  }
}

export default ReturnType;
