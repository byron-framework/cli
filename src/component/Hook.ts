import Leaf from 'src/component/Leaf';

import Generator from 'src/generators/Generator';

interface Timmings {
  pre?: HookTimming;
  post?: HookTimming;
}

interface HookTimming {
  except?: string[];
  only?: string[];
}

class Hook extends Leaf {
  public readonly name: string;
  public readonly type: string;
  public readonly pre?: HookTimming;
  public readonly post?: HookTimming;

  constructor(name: string, type: string, timmings: Timmings) {
    super();
    this.name = name;
    this.type = type;

    if (timmings.pre) {
      this.pre = timmings.pre;
    }

    if (timmings.post) {
      this.post = timmings.post;
    }
  }

  public accept(generator: Generator): void {
    generator.visitHook(this);
  }
}

export default Hook;
