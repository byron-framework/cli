import Leaf from 'src/component/Leaf';

import Command from 'src/component/Command';
import Entity from 'src/component/Entity';
import Input from 'src/component/Input';
import Generator from 'src/generators/Generator';

type Parent = Command | Entity | Input;

const PRIMITIVE_TYPES: string[] = [
  'String',
  'Int',
  'Float',
  'ID',
];

class Type extends Leaf {
  public readonly rootType: string;
  public readonly nullable: boolean;
  public readonly isArray: boolean = false;
  public readonly nullableArray: boolean = true;
  public readonly isForeignKey: boolean;
  public readonly parent: Parent;

  public constructor(originalType: string, parent: Parent) {
    super();

    this.parent = parent;

    if (originalType.match(/\[.*\]/)) {
      this.isArray = true;
      this.nullableArray = originalType[originalType.length - 1] !== '!';
    }

    const nullableIndex: number =
      originalType.length -
      Number(this.isArray) -
      Number(!this.nullableArray);

    this.nullable = originalType[nullableIndex - 1] !== '!';

    const finalTypeIndex: number = nullableIndex - Number(!this.nullable);
    this.rootType = originalType.slice(Number(this.isArray), finalTypeIndex);

    this.isForeignKey = !PRIMITIVE_TYPES.includes(this.rootType);
  }

  public accept(generator: Generator): void {
    generator.visitType(this);
  }
}

export default Type;
