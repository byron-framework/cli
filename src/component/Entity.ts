import InnerNode from 'src/component/InnerNode';

import Node from 'src/component/Node';
import Generator from 'src/generators/Generator';

class Entity extends InnerNode {
  public readonly name: string;

  public constructor(name: string) {
    super();

    this.name = name;
  }

  public accept(generator: Generator): void {
    generator.visitEntity(this);
    this.children.forEach((child: Node) => {
      child.accept(generator);
    });
  }
}

export default Entity;
