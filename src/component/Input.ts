import InnerNode from 'src/component/InnerNode';

import Node from 'src/component/Node';
import Generator from 'src/generators/Generator';

class Input extends InnerNode {
  public readonly name: string;

  public constructor(name: string) {
    super();

    this.name = name;
  }

  public accept(generator: Generator): void {
    generator.visitInput(this);
    this.children.forEach((child: Node): void => {
      child.accept(generator);
    });
  }
}

export default Input;
