import Leaf from 'src/component/Leaf';

import Generator from 'src/generators/Generator';

class APIConfig extends Leaf {
  public readonly port: number;

  public constructor(port: number) {
    super();

    this.port = port;
  }

  public accept(gen: Generator): void {
    gen.visitAPIConfig(this);
  }
}

export default APIConfig;
