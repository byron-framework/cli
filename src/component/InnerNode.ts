import Node from 'src/component/Node';

abstract class InnerNode extends Node {
  public readonly children: Node[] = [];

  public addChild(node: Node): void {
    this.children.push(node);
  }
}

export default InnerNode;
