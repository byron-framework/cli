import Node from 'src/component/Node';

abstract class Leaf extends Node {}

export default Leaf;
