import InnerNode from 'src/component/InnerNode';

import Node from 'src/component/Node';
import Generator from 'src/generators/Generator';

class Component extends InnerNode {
  public readonly name: string;
  public readonly namespace: string;

  constructor(name: string, namespace: string) {
    super();

    this.name = name;
    this.namespace = namespace;
  }

  public accept(generator: Generator): void {
    this.children.forEach((child: Node) => {
      child.accept(generator);
    });

    generator.visitComponent(this);
  }
}

export default Component;
