import { ChildProcessWithoutNullStreams, spawn } from 'child_process';

export type CompExecSignature =
(name: string, namespace: string, command: string) => void;

function compExec(name: string, namespace: string, command: string): void {
  const childProc: ChildProcessWithoutNullStreams = spawn(
    'docker-compose',
    ['-f', `/tmp/byron/${namespace}/${name}/docker-compose.yaml`, command],
  );

  childProc.stdout.on('data', (chunk: any): void => {
    process.stdout.write(chunk.toString());
  });

  childProc.stderr.on('data', (chunk: any): void => {
    process.stdout.write(chunk.toString());
  });
}

export default compExec;
