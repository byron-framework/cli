const checkRootDir: jest.Mock = jest.fn();
const checkSchema: jest.Mock = jest.fn();
const readSchema: jest.Mock = jest.fn();
const readTemplate: jest.Mock = jest.fn();
const writeGraphQLSchema: jest.Mock = jest.fn();
const writeMongoModel: jest.Mock = jest.fn();
const writeHandler: jest.Mock = jest.fn();
const copyHandler: jest.Mock = jest.fn();
const copyHook: jest.Mock = jest.fn();
const copyCommandHook: jest.Mock = jest.fn();
const copyHandlerHook: jest.Mock = jest.fn();
const writeCommandHooksAggregate: jest.Mock = jest.fn();
const writeHandlerHooksAggregate: jest.Mock = jest.fn();
const writeDockerCompose: jest.Mock = jest.fn();
const checkBuildDir: jest.Mock = jest.fn();
const makeBuildDirTree: jest.Mock = jest.fn();
const makeBuildBrokerDir: jest.Mock = jest.fn();
const removeBuildDir: jest.Mock = jest.fn();

const mock: any = {
  checkRootDir,
  checkSchema,
  readSchema,
  readTemplate,
  writeGraphQLSchema,
  writeMongoModel,
  writeHandler,
  copyHandler,
  copyHook,
  copyCommandHook,
  copyHandlerHook,
  writeCommandHooksAggregate,
  writeHandlerHooksAggregate,
  writeDockerCompose,
  checkBuildDir,
  makeBuildDirTree,
  makeBuildBrokerDir,
  removeBuildDir,
};

export default mock;
