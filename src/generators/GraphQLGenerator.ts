import Generator from 'src/generators/Generator';

import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';
import Input from 'src/component/Input';
import Parameter from 'src/component/Parameter';
import ReturnType from 'src/component/ReturnType';
import Type from 'src/component/Type';

import { render } from 'mustache';

class GraphQLGenerator extends Generator {
  private declaredTypes: any;
  private declaredResolvers: any;
  private FSWrapper: any;

  public constructor(fsWrapper: any) {
    super();

    this.FSWrapper = fsWrapper;

    this.declaredTypes = {};

    this.declaredResolvers = {};
  }

  public visitComponent(_: Component): void {
    const view: any = {
      types: this.formatTypeDeclarations(),
      resolvers: this.formatResolverDeclarations(),
    };

    const schema: string = render(this.loadTemplate('schema.template.graphql'), view, {
      type: this.loadTemplate('type.template.graphql'),
      resolver: this.loadTemplate('resolver.template.graphql'),
    });

    this.FSWrapper.writeGraphQLSchema(schema);
  }

  public visitEntity(entity: Entity): void {
    this.declaredTypes[entity.name] = {
      declarationGroup: 'type',
      attributes: [],
    };
  }

  public visitInput(input: Input): void {
    this.declaredTypes[input.name] = {
      declarationGroup: 'input',
      attributes: [],
    };
  }

  public visitField(field: Field): void {
    this.declaredTypes[field.parent.name].attributes.push({
      name: field.name,
      type: this.formatType(field),
    });
  }

  public visitCommand(command: Command): void {
    this.declaredResolvers[command.name] = {
      type: command.type,
      params: [],
    };
  }

  public visitParameter(parameter: Parameter): void {
    this.declaredResolvers[parameter.parent.name].params.push({
      name: parameter.name,
      type: this.formatType(parameter),
      isNotLast: true,
    });
  }

  public visitReturnType(returnType: ReturnType): void {
    this.declaredResolvers[returnType.parent.name].returnType = this.formatType(returnType);
  }

  private formatType(type: Type): string {
    let formattedType: string = type.rootType;
    formattedType = type.nullable ? formattedType : formattedType + '!';
    formattedType = type.isArray ? '[' + formattedType + ']' : formattedType;
    formattedType = type.nullableArray ? formattedType : formattedType + '!';

    return formattedType;
  }

  private formatTypeDeclarations(): any[] {
    const types: any[] = [];

    Object.keys(this.declaredTypes)
      .forEach((key: string): void => {
        types.push({
          name: key,
          ...this.declaredTypes[key],
        });
      });

    return types;
  }

  private formatResolverDeclarations(): any[] {
    const resolvers: any[] = [];

    Object.keys(this.declaredResolvers)
      .forEach((key: string): void => {
        const resolver: any = {
          name: key,
          ...this.declaredResolvers[key],
        };

        if (resolver.params.length) {
          resolver.params[resolver.params.length - 1].isNotLast = false;
        }

        const categoryIndex: number =
          resolvers.findIndex((item: any) => item.category === this.declaredResolvers[key].type);

        if (categoryIndex === -1) {
          const resolversCategory: any = {
            category: this.declaredResolvers[key].type,
            resolvers: [resolver],
          };

          resolvers.push(resolversCategory);
        } else {
          resolvers[categoryIndex].resolvers.push(resolver);
        }
      });

    return resolvers;
  }

  private loadTemplate(template: string): string {
    return this.FSWrapper.readTemplate(['GraphQL', template]);
  }
}

export default GraphQLGenerator;
