import Generator from './Generator';

import ValidationError from 'src/errors/ValidationError';

import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';
import Handler from 'src/component/Handler';
import Hook from 'src/component/Hook';
import Input from 'src/component/Input';

class Validator extends Generator {
  private entitiesNames: string[] = [];
  private inputsNames: string[] = [];
  private fieldsPerParent: { [key: string]: string[] } = {};
  private commandsNames: string[] = [];
  private queriesCount: number = 0;
  private usedTypes: string[] = [];
  private handlersNames: string[] = [];

  public visitCommand(command: Command): void {
    if (command.name === '') {
      throw new ValidationError('Commands name cannot be blank');
    }

    if (!this.commandsNames.includes(command.name)) {
      this.commandsNames.push(command.name);
    } else {
      throw new ValidationError(`Command name must be unique [${command.name}]`);
    }

    const validTypes: string[] = ['Query', 'Mutation', 'Subscription'];

    if (!validTypes.includes(command.type)) {
      throw new ValidationError(`Command type [${command.type}] is not 'Query', 'Mutation' or 'Subscription'`);
    }

    if (command.type === 'Query') {
      this.queriesCount++;
    }
  }

  public visitComponent(_component: Component): void {
    if (this.queriesCount === 0) {
      throw new ValidationError('Component must have at least 1 command of type Query');
    }

    const knownTypes: string[] = [
      'String',
      'Int',
      'Float',
      'Boolean',
      'ID',
      ...this.entitiesNames,
      ...this.inputsNames,
    ];

    this.usedTypes
      .forEach((used: string): void => {
        if (!knownTypes.includes(used)) {
          throw new ValidationError(`Unknown type [${used}]`);
        }
      });
  }

  public visitEntity(entity: Entity): void {
    if (entity.name === '') {
      throw new ValidationError('Entity name cannot be blank');
    }

    if (!this.entitiesNames.includes(entity.name)) {
      this.entitiesNames.push(entity.name);
    } else {
      throw new ValidationError(`Entity name must be unique [${entity.name}]`);
    }

    if (entity.children.length === 0) {
      throw new ValidationError('Entities must have at least one Field');
    }
  }

  public visitField(field: Field): void {
    if (field.name === '') {
      throw new ValidationError('Field name cannot be blank');
    }

    if (!Object
        .keys(this.fieldsPerParent)
        .includes(field.parent.name)) {
      this.fieldsPerParent[field.parent.name] = [];
    }

    if (!this.fieldsPerParent[field.parent.name].includes(field.name)) {
      this.fieldsPerParent[field.parent.name].push(field.name);
    } else {
      throw new ValidationError(`Fields of a Input or Entity must have unique names [${field.name}]`);
    }

    this.usedTypes.push(field.rootType);
  }

  public visitInput(input: Input): void {
    if (input.name === '') {
      throw new ValidationError('Input name cannot be blank');
    }

    if (!this.inputsNames.includes(input.name)) {
      this.inputsNames.push(input.name);
    } else {
      throw new ValidationError(`Input name must be unique [${input.name}]`);
    }

    if (input.children.length === 0) {
      throw new ValidationError('Inputs must have at least one Field');
    }
  }

  public visitHandler(handler: Handler): void {
    if (!this.handlersNames.includes(handler.name)) {
      this.handlersNames.push(handler.name);
    } else {
      throw new ValidationError(`Handler name must be unique [${handler.name}]`);
    }
  }

  public visitHook(hook: Hook): void {
    const validTypes: string[] = ['commandHook', 'handlerHook'];

    if (!validTypes.includes(hook.type)) {
      throw new ValidationError(`Unknown hook type [${hook.type}]`);
    }
  }
}

export default Validator;
