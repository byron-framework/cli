import Generator from 'src/generators/Generator';

import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';

import Mustache, { render } from 'mustache';

Mustache.escape = (text: string): string => text;

interface ModelAttribute {
  name: string;
  type: string;
}

interface DeclaredModels {
  [key: string]: ModelAttribute[];
}

const TYPES_MAPPING: { [key: string]: string } = {
  Int: 'Number',
  Float: 'Number',
  String: 'String',
  ID: 'String',
  Boolean: 'Boolean',
};

class MongoModelGenerator extends Generator {
  private declaredModels: DeclaredModels;
  private FSWrapper: any;

  public constructor(fsWrapper: any) {
    super();

    this.declaredModels = {};
    this.FSWrapper = fsWrapper;
  }

  public visitComponent(_: Component): void {
    const template: string =
      this.FSWrapper.readTemplate(['Mongo', 'model.template.ts']);

    Object
      .keys(this.declaredModels)
      .forEach((modelName: string) => {
        const view: { [key: string]: string | ModelAttribute[] } = {
          name: modelName,
          attrs: this.declaredModels[modelName],
        };

        const model: string = render(template, view);

        this.FSWrapper.writeMongoModel(modelName, model);

      });
  }

  public visitEntity(entity: Entity): void {
    this.declaredModels[entity.name] = [];
  }

  public visitField(field: Field): void {
    if (field.parent instanceof Entity) {
      let type: string;

      if (field.isForeignKey) {
        type = `{ type: String, ref: '${field.rootType}' }`;
      } else {
        type = TYPES_MAPPING[field.rootType];
      }

      type = field.isArray ? `[${type}]` : type;

      this.declaredModels[field.parent.name].push({
        name: field.name,
        type,
      });
    }
  }
}

export default MongoModelGenerator;
