import APIConfig from 'src/component/APIConfig';
import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';
import Handler from 'src/component/Handler';
import Hook from 'src/component/Hook';
import Input from 'src/component/Input';
import Parameter from 'src/component/Parameter';
import ReturnType from 'src/component/ReturnType';
import Type from 'src/component/Type';

class Generator {
  public visitComponent(_: Component): void {
    return;
  }

  public visitCommand(_: Command): void {
    return;
  }

  public visitField(_: Field): void {
    return;
  }

  public visitEntity(_: Entity): void {
    return;
  }

  public visitInput(_: Input): void {
    return;
  }

  public visitType(_: Type): void {
    return;
  }

  public visitParameter(_: Parameter): void {
    return;
  }

  public visitReturnType(_: ReturnType): void {
    return;
  }

  public visitHandler(_: Handler): void {
    return;
  }

  public visitHook(_: Hook): void {
    return;
  }

  public visitAPIConfig(_: APIConfig): void {
    return;
  }
}

export default Generator;
