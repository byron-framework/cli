import Generator from 'src/generators/Generator';

import Component from 'src/component/Component';
import Hook from 'src/component/Hook';

import FSWrapper from 'src/FSWrapper';

class HooksGenerator extends Generator {
  private commandHooks: any = {};
  private handlerHooks: any = {};

  constructor(private fs: FSWrapper) {
    super();
  }

  public visitComponent(_: Component): void {
    const commandAggregate: string = JSON.stringify(this.commandHooks);
    const handlerAggregate: string = JSON.stringify(this.handlerHooks);

    this.fs.writeCommandHooksAggregate(commandAggregate);
    this.fs.writeHandlerHooksAggregate(handlerAggregate);
  }

  public visitHook(hook: Hook): void {
    const obj: any = {};

    if (hook.pre !== undefined) {
      obj.pre = hook.pre;
    }

    if (hook.post !== undefined) {
      obj.post = hook.post;
    }

    if (hook.type === 'commandHook') {
      this.fs.copyCommandHook(hook.name);
      this.commandHooks[hook.name] = obj;
    } else if (hook.type === 'handlerHook') {
      this.fs.copyHandlerHook(hook.name);
      this.handlerHooks[hook.name] = obj;
    }
  }
}

export default HooksGenerator;
