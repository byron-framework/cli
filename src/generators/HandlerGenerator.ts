import Generator from 'src/generators/Generator';

import Component from 'src/component/Component';
import Handler from 'src/component/Handler';

import { render } from 'mustache';

interface HandlerInfo {
  name: string;
  event: string;
}

class HandlersGenerator extends Generator {
  private declaredHandlers: HandlerInfo[];
  private FSWrapper: any;

  public constructor(fsWrapper: any) {
    super();

    this.declaredHandlers = [];
    this.FSWrapper = fsWrapper;
  }

  public visitComponent(_: Component): void {
    const template: string =
      this.FSWrapper.readTemplate(['Handler', 'handler.template.ts']);

    this.declaredHandlers
      .forEach((view: HandlerInfo) => {
        const handler: string = render(template, view);

        this.FSWrapper.copyHandler(view.name);
        this.FSWrapper.writeHandler(view.name, handler);
      });
  }

  public visitHandler(handler: Handler): void {
    this.declaredHandlers.push({
      name: handler.name,
      event: handler.event,
    });
  }
}

export default HandlersGenerator;
