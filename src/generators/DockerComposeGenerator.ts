import Generator from 'src/generators/Generator';

import APIConfig from 'src/component/APIConfig';
import Component from 'src/component/Component';

import { render } from 'mustache';

interface DockerComposeView {
  serviceName: string;
  namespace: string;
  apiPort: number;
}

class DockerComposeGenerator extends Generator {
  private FSWrapper: any;
  private apiPort: number;

  public constructor(fsWrapper: any) {
    super();

    this.FSWrapper = fsWrapper;
    this.apiPort = 4000;
  }

  public visitComponent(comp: Component): void {
    const view: DockerComposeView = {
      serviceName: comp.name.toLowerCase(),
      namespace: comp.namespace.toLowerCase(),
      apiPort: this.apiPort,
    };

    const template: string = this.FSWrapper.readTemplate([
      'DockerCompose',
      'component.template.yaml',
    ]);

    this.FSWrapper.writeDockerCompose(render(template, view));
  }

  public visitAPIConfig(config: APIConfig): void {
    this.apiPort = config.port;
  }
}

export default DockerComposeGenerator;
