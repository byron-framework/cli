import { YAMLConfig } from 'src/typings/interpreters/YAML/interpreter';

import APIConfig from 'src/component/APIConfig';
import Component from 'src/component/Component';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

function interpretAPIConfig(config: YAMLConfig, comp: Component): any {
  if (!config.api) {
    throw new SchemaSyntaxError('Config\'s api cannot be null');
  }

  if (!config.api.port) {
    throw new SchemaSyntaxError('API config\'s port cannot be null');
  }

  if (typeof config.api.port !== 'number') {
    throw new SchemaSyntaxError('API config\'s port can only be a number');
  }

  comp.addChild(new APIConfig(config.api.port));
}

export default interpretAPIConfig;
