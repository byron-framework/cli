import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Parameter from 'src/component/Parameter';
import ReturnType from 'src/component/ReturnType';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import {
  YAMLCommand,
  YAMLParameter,
} from 'src/typings/interpreters/YAML/interpreter';

function interpretCommands(commands: YAMLCommand[], comp: Component): void {
  commands.forEach((cmd: YAMLCommand) => {
    if (!cmd.name) {
      throw new SchemaSyntaxError('Command\'s name cannot be null');
    }

    if (!cmd.type) {
      throw new SchemaSyntaxError(
        `Command\'s type cannot be null, in command: ${cmd.name}`,
      );
    }

    if (!cmd.returnType) {
      throw new SchemaSyntaxError(
        `Command\'s returnType cannot be null, in command: ${cmd.name}`,
      );
    }

    if (cmd.parameters && !Array.isArray(cmd.parameters)) {
      throw new SchemaSyntaxError(
        `Command's parameters cannot be other type than array, in command: ${cmd.name}`,
      );
    }

    const command: Command = new Command(cmd.name, cmd.type);
    if (cmd.parameters) {
      cmd.parameters
        .forEach((pmt: YAMLParameter): void => {
          if (!pmt.name) {
            throw new SchemaSyntaxError(
              `Parameter's name cannot be null, in command: ${cmd.name}`,
            );
          }

          if (!pmt.type) {
            throw new SchemaSyntaxError(
              `Parameter's type cannot be null, in command: ${cmd.name}, in parameter: ${pmt.name}`,
            );
          }

          command.addChild(new Parameter(pmt.name, pmt.type, command));
        });
    }

    command.addChild(new ReturnType(cmd.returnType, command));

    comp.addChild(command);
  });
}

export default interpretCommands;
