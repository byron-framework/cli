import { safeLoad } from 'js-yaml';

import FileSystemError from 'src/errors/FileSystemError';
import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import Component from 'src/component/Component';

import {
  YAMLComponent,
} from 'src/typings/interpreters/YAML/interpreter';

import interpretAPIConfig from 'src/interpreters/YAML/APIConfig';
import interpretCommands from 'src/interpreters/YAML/commands';
import interpretEntities from 'src/interpreters/YAML/entities';
import interpretHandlers from 'src/interpreters/YAML/handlers';
import interpretInputs from 'src/interpreters/YAML/inputs';

export function interpret(FSW: any): Component {
  const fsw: any = FSW.instance;

  const hasYAML: boolean = fsw.checkSchema('schema.yaml');
  const hasYML: boolean = fsw.checkSchema('schema.yml');

  if (!hasYAML && !hasYML) {
    throw new FileSystemError();
  }

  const schema: string = fsw.readSchema();

  const parsedSchema: YAMLComponent = safeLoad(schema);

  if (!parsedSchema.name && !parsedSchema.namespace && !parsedSchema.content) {
    throw new SchemaSyntaxError();
  }

  if (!(parsedSchema.content instanceof Object) || Array.isArray(parsedSchema.content)) {
    throw new SchemaSyntaxError();
  }

  const { name, namespace }: YAMLComponent = parsedSchema;
  const comp: Component = new Component(name, namespace);

  const schemaContentKeys: string[] = Object
    .keys(parsedSchema.content);

  if (schemaContentKeys.includes('types')) {
    interpretEntities(parsedSchema.content.types, comp);
  }

  if (schemaContentKeys.includes('inputs')) {
    interpretInputs(parsedSchema.content.inputs, comp);
  }

  if (schemaContentKeys.includes('commands')) {
    interpretCommands(parsedSchema.content.commands, comp);
  }

  if (schemaContentKeys.includes('handlers')) {
    interpretHandlers(parsedSchema.content.handlers, comp);
  }

  if (parsedSchema.config) {
    interpretAPIConfig(parsedSchema.config, comp);
  }

  return comp;
}
