import Component from 'src/component/Component';
import Handler from 'src/component/Handler';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLHandler } from 'src/typings/interpreters/YAML/interpreter';

function interpretHandlers(handlers: YAMLHandler[], comp: Component): void {
  handlers.forEach((hdl: YAMLHandler) => {
    if (!hdl.name) {
      throw new SchemaSyntaxError('Handler\'s name cannot be null');
    }

    if (!hdl.event) {
      throw new SchemaSyntaxError(`Handler's event cannot be null, in handler: ${hdl.name}`);
    }

    comp.addChild(new Handler(hdl.name, hdl.event));
  });
}

export default interpretHandlers;
