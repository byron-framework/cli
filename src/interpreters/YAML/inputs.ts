import Component from 'src/component/Component';
import Field from 'src/component/Field';
import Input from 'src/component/Input';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import {
  YAMLAttribute,
  YAMLInput,
} from 'src/typings/interpreters/YAML/interpreter';

function interpretInputs(inputs: YAMLInput[], comp: Component): void {
  inputs.forEach((inp: YAMLInput): void => {
    if (!inp.name) {
      throw new SchemaSyntaxError('Input\'s name cannot be null');
    }

    if (!inp.attributes) {
      throw new SchemaSyntaxError(`Input's attributes cannot be null, in input: ${inp.name}`);
    }

    if (!Array.isArray(inp.attributes)) {
      throw new SchemaSyntaxError(
        `Input's attributes cannot be other type than array, in input: ${inp.name}`,
      );
    }

    const input: Input = new Input(inp.name);

    inp.attributes
      .forEach((attr: YAMLAttribute): void => {
        if (!attr.name) {
          throw new SchemaSyntaxError(
            `Attribute's name cannot be null, in input: ${inp.name}`,
          );
        }

        if (!attr.type) {
          throw new SchemaSyntaxError(
            `Attribute's type cannot be null, in input: ${inp.name}, in attribute: ${attr.name}`,
          );
        }

        input.addChild(new Field(attr.name, attr.type, input));
      });

    comp.addChild(input);
  });
}

export default interpretInputs;
