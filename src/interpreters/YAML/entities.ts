import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import {
  YAMLAttribute,
  YAMLEntity,
} from 'src/typings/interpreters/YAML/interpreter';

function interpretEntities(types: YAMLEntity[], comp: Component): void {
  types.forEach((el: YAMLEntity): void => {
    if (!el.name) {
      throw new SchemaSyntaxError('Type\'s name cannot be null');
    }

    if (!el.attributes) {
      throw new SchemaSyntaxError(`Type's attributes cannot be null, in type: ${el.name}`);
    }

    if (!Array.isArray(el.attributes)) {
      throw new SchemaSyntaxError(
        `Type's attributes cannot be other type than array, in type: ${el.name}`,
      );
    }

    const entity: Entity = new Entity(el.name);

    el.attributes.push({
      name: '_id',
      type: 'ID!',
    });

    el.attributes
      .forEach((attr: YAMLAttribute): void => {
        if (!attr.name) {
          throw new SchemaSyntaxError(
            `Attribute's name cannot be null, in type: ${el.name}`,
          );
        }

        if (!attr.type) {
          throw new SchemaSyntaxError(
            `Attribute's type cannot be null, in type: ${el.name}, in attribute: ${attr.name}`,
          );
        }

        entity.addChild(new Field(attr.name, attr.type, entity));
      });

    comp.addChild(entity);
  });
}

export default interpretEntities;
