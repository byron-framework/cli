import { down } from 'src/commands/component/down';

import jsyaml from 'js-yaml';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';
import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

jest.mock('js-yaml');
jest.mock('child_process');

const schema: string =
`name: User
namespace: Byron
`;

describe('component down command', () => {
  const fswMock: any = FSWrapper;

  const compExec: (
    name: string,
    namespace: string,
    command: string) => void = jest.fn();

  const defaultArgs: string[] = [
    'node',
    'byron',
    'component',
    'down',
  ];

  describe('No path is passed', () => {
    beforeEach(() => {
      process.argv = [...defaultArgs];
    });

    it('should throw missing argument', () => {
      expect(down)
        .toThrow();
    });
  });

  describe('when called with help flag', () => {
    beforeEach(() => {
      process.argv = [ ...defaultArgs, '--help' ];
      console.log = jest.fn();
      down(fswMock, compExec);
    });

    it('should render help menu', () => {
      expect(console.log)
        .toBeCalledWith('Component Down Help Menu');
    });
  });

  describe('when called with path argument', () => {
    beforeEach(() => {
      process.argv = [...defaultArgs, 'User'];
    });

    it('should not throw an error', () => {
      expect(down)
        .not
        .toThrowError(ArgumentMissingError);
    });

    describe('when path does not exist', () => {
      beforeEach(() => {
        fswMock.checkRootDir = jest
          .fn()
          .mockReturnValueOnce(false);
      });

      it('should throw FileSystemError', () => {
        expect(() => down(fswMock, compExec))
          .toThrowError(FileSystemError);
      });
    });

    describe('when path does exist', () => {
      beforeEach(() => {
        fswMock.checkRootDir = jest
          .fn()
          .mockReturnValueOnce(true);
      });

      describe('when schema does not exist', () => {
        beforeEach(() => {
          fswMock.checkSchema = jest
            .fn()
            .mockReturnValueOnce(false);
        });

        it('should throw FileSystemError', () => {
          expect(() => down(fswMock, compExec))
            .toThrowError(FileSystemError);
        });
      });

      describe('when schema does exist', () => {
        beforeEach(() => {
          process.argv = [ ...defaultArgs, 'User' ];

          fswMock.checkRootDir = jest
            .fn()
            .mockReturnValueOnce(true);

          fswMock.checkSchema = jest
            .fn()
            .mockReturnValueOnce(true);

          fswMock.readSchema = jest
            .fn()
            .mockReturnValueOnce(schema);

        });

        describe('when schema is invalid', () => {
          describe('when there is no name', () => {
            beforeEach(() => {
              jsyaml.safeLoad = jest
                .fn()
                .mockReturnValue({ namespace: 'Byron' });
            });

            it('should throw SchemaSyntax error', () => {
              expect(() => down(fswMock, compExec))
                .toThrowError(SchemaSyntaxError);
            });
          });

          describe('when there is no namespace', () => {
            beforeEach(() => {
              jsyaml.safeLoad = jest
                .fn()
                .mockReturnValue({ name: 'User' });
            });

            it('should throw SchemaSyntax error', () => {
              expect(() => down(fswMock, compExec))
                .toThrowError(SchemaSyntaxError);
            });
          });
        });

        describe('when schema is valid', () => {
          beforeEach(() => {
            jsyaml.safeLoad = jest
              .fn()
              .mockReturnValue({ name: 'User', namespace: 'Byron' });

            down(fswMock, compExec);
          });

          it('should call  with given path', () => {
            expect(compExec)
              .toBeCalledWith('User', 'Byron', 'down');
          });
        });

      });
    });
  });
});
