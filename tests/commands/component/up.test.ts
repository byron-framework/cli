import { up } from 'src/commands/component/up';

import jsyaml from 'js-yaml';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';
import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

jest.mock('js-yaml');

const schema: string =
`name: User
namespace: Byron
`;

describe('component up command', () => {
  const fswMock: any = FSWrapper;

  const compExec: (
    name: string,
    namespace: string,
    command: string) => void = jest.fn();

  const defaultArgs: string[] = [
    'node',
    'byron',
    'component',
    'up',
  ];

  describe('No path is passed', () => {
    beforeEach(() => {
      process.argv = [...defaultArgs];
    });

    it('should throw missing argument', () => {
      expect(up)
        .toThrow();
    });
  });

  describe('when called with help flag', () => {
    beforeEach(() => {
      process.argv = [ ...defaultArgs, '--help' ];
      console.log = jest.fn();
      up(fswMock, compExec);
    });

    it('should render help menu', () => {
      expect(console.log)
        .toBeCalledWith('Component Init Help Menu');
    });
  });

  describe('when called with path argument', () => {
    beforeEach(() => {
      process.argv = [...defaultArgs, 'User'];
    });

    it('should not throw an error', () => {
      expect(up)
        .not
        .toThrowError(ArgumentMissingError);
    });

    describe('when path does not exist', () => {
      beforeEach(() => {
        fswMock.checkRootDir = jest
          .fn()
          .mockReturnValueOnce(false);
      });

      it('should throw FileSystemError', () => {
        expect(() => up(fswMock, compExec))
          .toThrowError(FileSystemError);
      });
    });

    describe('when path does exist', () => {
      beforeEach(() => {
        fswMock.checkRootDir = jest
          .fn()
          .mockReturnValueOnce(true);
      });

      describe('when schema does not exist', () => {
        beforeEach(() => {
          fswMock.checkSchema = jest
            .fn()
            .mockReturnValueOnce(false);
        });

        it('should throw FileSystemError', () => {
          expect(() => up(fswMock, compExec))
            .toThrowError(FileSystemError);
        });
      });

      describe('when schema does exist', () => {
        beforeEach(() => {
          process.argv = [ ...defaultArgs, 'User' ];

          fswMock.checkRootDir = jest
            .fn()
            .mockReturnValueOnce(true);

          fswMock.checkSchema = jest
            .fn()
            .mockReturnValueOnce(true);

          fswMock.readSchema = jest
            .fn()
            .mockReturnValueOnce(schema);

        });

        describe('when schema is invalid', () => {
          describe('when there is no name', () => {
            beforeEach(() => {
              jsyaml.safeLoad = jest
                .fn()
                .mockReturnValue({ namespace: 'Byron' });
            });

            it('should throw SchemaSyntax error', () => {
              expect(() => up(fswMock, compExec))
                .toThrowError(SchemaSyntaxError);
            });
          });

          describe('when there is no namespace', () => {
            beforeEach(() => {
              jsyaml.safeLoad = jest
                .fn()
                .mockReturnValue({ name: 'User' });
            });

            it('should throw SchemaSyntax error', () => {
              expect(() => up(fswMock, compExec))
                .toThrowError(SchemaSyntaxError);
            });
          });
        });

        describe('when schema is valid', () => {
          beforeEach(() => {
            jsyaml.safeLoad = jest
              .fn()
              .mockReturnValue({ name: 'User', namespace: 'Byron' });

            up(fswMock, compExec);
          });

          it('should call with given path', () => {
            expect(compExec)
              .toBeCalledWith('User', 'Byron', 'up');
          });
        });

      });
    });
  });
});
