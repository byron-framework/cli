import { init } from 'src/commands/component/init';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

// tslint:disable-next-line: no-var-requires
const fs: any = require('fs-extra');
jest.mock('fs-extra');

const schema: string =
`name: User
namespace: Byron
`;

describe('component init command', () => {
  const defaultArgs: string[] = [
    'node',
    'byron',
    'component',
  ];

  const componentInitArgs: string[] = [
    ...defaultArgs,
    'init',
  ];

  const componentInitHelpMenu: string = `Component Init Help Menu`;

  describe('when called with local help flag', () => {
    beforeEach(() => {
      process.argv = [
        ...componentInitArgs,
        '--help',
      ];
      console.log = jest.fn();
      init();
    });

    it('should display help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(componentInitHelpMenu);
    });
  });

  describe('when called with missing args', () => {
    beforeEach(() => {
      process.argv = [...componentInitArgs];
    });

    it('should throw ArgumentMissingError', () => {
      expect(init)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called with wrong name flag', () => {
    beforeEach(() => {
      process.argv = [...componentInitArgs, '-s', 'User', '-N', 'Byron'];
    });

    it('should throw ArgumentMissingError', () => {
      expect(init)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called with wrong namespace flag', () => {
    beforeEach(() => {
      process.argv = [...componentInitArgs, '-n', 'User', '-S', 'Byron'];
    });

    it('should throw ArgumentMissingError', () => {
      expect(init)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called correctly with short args', () => {
    beforeEach(() => {
      process.argv = [
        ...componentInitArgs,
        '-n',
        'User',
        '-N',
        'Byron',
      ];
    });

    it('should not throw ArgumentMissingError', () => {
      expect(init)
        .not
        .toThrowError(ArgumentMissingError);
    });

    describe('when directory already exists', () => {
      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(true);
      });

      it('should throw a FileSystemError', () => {
        expect(init)
          .toThrowError(FileSystemError);
      });
    });

    describe('when directory does not exist', () => {
      const path: any = require('path');

      beforeEach(() => {
        fs.existsSync = jest.fn();
        fs.mkdirSync = jest.fn();
        fs.writeFileSync = jest.fn();

        fs.existsSync
          .mockReturnValueOnce(false);

        path.resolve = jest.fn()
          .mockImplementation((...args: string[]) => args.join('/'));

        init();
      });

      it('should create a directory with given name in the current directory', () => {
        const pathToComp: string = `${process.cwd()}/User`;
        expect(fs.mkdirSync)
          .toHaveBeenCalledWith(pathToComp);
      });

      it('should create the standard directory structure', () => {
        const basePath: string = `${process.cwd()}/User`;
        const directories: string[] = [
          'api',
          'api/commands',
          'hooks',
          'sink',
          'sink/handlers',
        ];

        for (const dir of directories) {
          expect(fs.mkdirSync)
            .toHaveBeenCalledWith(`${basePath}/${dir}`);
        }

        expect(fs.writeFileSync)
          .toHaveBeenCalledWith(`${basePath}/schema.yml`, schema);
      });
    });
  });

  describe('when called correctly with long args', () => {
    beforeEach(() => {
      process.argv = [
        ...componentInitArgs,
        '--name',
        'User',
        '--namespace',
        'Byron',
      ];
    });

    it('should not throw ArgumentMissingError', () => {
      expect(init)
        .not
        .toThrowError(ArgumentMissingError);
    });

    describe('when directory already exists', () => {
      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(true);
      });

      it('should throw a FileSystemError', () => {
        expect(init)
          .toThrowError(FileSystemError);
      });
    });

    describe('when directory does not exist', () => {
      const path: any = require('path');

      beforeEach(() => {
        fs.existsSync = jest.fn();
        fs.mkdirSync = jest.fn();
        fs.writeFileSync = jest.fn();

        fs.existsSync
          .mockReturnValueOnce(false);

        path.resolve = jest.fn()
          .mockImplementation((...args: string[]) => args.join('/'));

        init();
      });

      it('should create a directory with given name in the current directory', () => {
        const pathToComp: string = `${process.cwd()}/User`;
        expect(fs.mkdirSync)
          .toHaveBeenCalledWith(pathToComp);
      });

      it('should create the standard directory structure', () => {
        const basePath: string = `${process.cwd()}/User`;
        const directories: string[] = [
          'api',
          'api/commands',
          'hooks',
          'sink',
          'sink/handlers',
        ];

        for (const dir of directories) {
          expect(fs.mkdirSync)
            .toHaveBeenCalledWith(`${basePath}/${dir}`);
        }

        expect(fs.writeFileSync)
          .toHaveBeenCalledWith(`${basePath}/schema.yml`, schema);
      });
    });
  });
});
