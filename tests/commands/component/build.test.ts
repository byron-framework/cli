import { build } from 'src/commands/component/build';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

import { interpret } from 'src/interpreters/YAML/interpreter';

jest.mock('src/interpreters/YAML/interpreter');

describe('component build command', () => {
  const componentBuildArgs: string[] = [
    'node',
    'byron',
    'component',
    'build',
  ];

  const componentBuildHelpMenu: string = `Component Build Help Menu`;

  describe('when called with local help flag', () => {
    beforeEach(() => {
      process.argv = [
        ...componentBuildArgs,
        '--help',
      ];
      console.log = jest.fn();
      build();
    });

    it('should display help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(componentBuildHelpMenu);
    });
  });

  describe('when called without any arguments', () => {
    beforeEach(() => {
      process.argv = [...componentBuildArgs];
    });

    it('throw an ArgumentMissingError', () => {
      expect(build)
        .toThrow(ArgumentMissingError);
    });
  });

  describe('when called with an argument', () => {
    const givenPath: string = 'Path';

    const buildWithArgs: string[] = [
      ...componentBuildArgs,
      givenPath,
    ];

    const fswMock: any = {
      instance: {
        checkRootDir: jest.fn(),
      },
    };

    beforeEach(() => {
      process.argv = [...buildWithArgs];
    });

    it('should not throw any error', () => {
      expect(build)
        .not
        .toThrow(ArgumentMissingError);
    });

    describe('when given path does not exist', () => {
      beforeEach(() => {
        process.argv = [...buildWithArgs];
        fswMock.instance.checkRootDir
          .mockReturnValueOnce(false);
      });

      it('should throw a FileSystemError', () => {
        expect(() => build(fswMock))
          .toThrow(FileSystemError);
      });
    });

    describe('when given path exists', () => {
      let accept: any;

      beforeEach(() => {
        fswMock.instance.checkRootDir
          .mockReturnValueOnce(true);

        fswMock.instance.makeBuildDirTree = jest.fn();
        fswMock.instance.removeBuildDir = jest.fn();
        fswMock.instance.checkBuildDir = jest.fn(() => true);

        accept = jest.fn();

        (interpret as jest.Mock<any, any>)
          .mockReturnValueOnce({
            accept,
          });

        build(fswMock);
      });

      it('should call remove', () => {
        expect(fswMock.instance.removeBuildDir)
          .toHaveBeenCalled();
      });

      it('should call interpret', () => {
        expect(interpret)
          .toHaveBeenCalled();
      });

      it('should call Validation Visitor', () => {
        expect(accept)
          .toHaveBeenCalled();
      });
    });

    describe('when given path exists', () => {
      let accept: any;

      beforeEach(() => {
        fswMock.instance.checkRootDir
          .mockReturnValueOnce(true);

        fswMock.instance.makeBuildDirTree = jest.fn();
        fswMock.instance.removeBuildDir = jest.fn();
        fswMock.instance.checkBuildDir = jest.fn(() => false);

        accept = jest.fn();

        (interpret as jest.Mock<any, any>)
          .mockReturnValueOnce({
            accept,
          });

        build(fswMock);
      });

      it('should not call remove', () => {
        expect(fswMock.instance.removeBuildDir)
          .not
          .toHaveBeenCalled();
      });
    });
  });
});
