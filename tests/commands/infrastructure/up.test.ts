import { up } from 'src/commands/infrastructure/up';

import { readFileSync } from 'fs-extra';
import { resolve } from 'path';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';

const yaml: string =
`version: '3.5'

services:
  broker:
    image: nats-streaming:0.16.2-linux
    ports:
      - 8222:8222
    networks:
      - byron_services
    volumes:
      - "data_lake:/datastore"
    command: "-st FILE --dir /datastore -m 8222"

volumes:
  data_lake:

networks:
  byron_services:
    name: byron_mesh`;

function readTemplate(templatePath: string[]): string {
  const path: string =
    resolve(__dirname, '..', '..', '..', 'templates', ...templatePath);
  return readFileSync(path)
    .toString();
}

describe('component init command', () => {
  const fswMock: any = {
    instance: {
      makeBuildBrokerDir: jest.fn(),
      writeDockerCompose: jest.fn(),
      readTemplate,
    },
  };

  const compExec: (
    name: string,
    namespace: string,
    command: string) => void = jest.fn();

  const defaultArgs: string[] = [
    'node',
    'byron',
    'infrastructure',
  ];

  const infrastructureUpArgs: string[] = [
    ...defaultArgs,
    'up',
  ];

  const infrastructureUpHelpMenu: string = `Infrastructure Up Help Menu`;

  describe('when called with local help flag', () => {
    beforeEach(() => {
      process.argv = [ ...infrastructureUpArgs, '--help' ];
      console.log = jest.fn();
      up();
    });

    it('should display help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(infrastructureUpHelpMenu);
    });
  });

  describe('when called with missing args', () => {
    beforeEach(() => {
      process.argv = [...infrastructureUpArgs];
    });

    it('should throw ArgumentMissingError', () => {
      expect(up)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called with wrong flag', () => {
    beforeEach(() => {
      process.argv = [...infrastructureUpArgs, '-S', 'Byron'];
    });

    it('should throw ArgumentMissingError', () => {
      expect(up)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called correctly with short args', () => {
    const path: any = require('path');

    beforeEach(() => {
      process.argv = [ ...infrastructureUpArgs, '-N', 'Byron' ];

      path.resolve = jest.fn()
        .mockImplementation((...args: string[]) => args.join('/'));

      up(fswMock, compExec);
    });

    it('should create docker-compose correctly', () => {
      expect(fswMock.instance.makeBuildBrokerDir)
        .toBeCalledWith('Byron', 'Broker');

      expect(fswMock.instance.writeDockerCompose)
        .toBeCalledWith(yaml);

      expect(compExec)
        .toBeCalledWith('Broker', 'Byron', 'up');
    });
  });

  describe('when called correctly with long args', () => {
    const path: any = require('path');

    beforeEach(() => {
      process.argv = [ ...infrastructureUpArgs, '--namespace', 'Byron' ];

      path.resolve = jest.fn()
        .mockImplementation((...args: string[]) => args.join('/'));

      up(fswMock, compExec);
    });

    it('should create docker-compose correctly', () => {
      expect(fswMock.instance.makeBuildBrokerDir)
        .toBeCalledWith('Byron', 'Broker');

      expect(fswMock.instance.writeDockerCompose)
        .toBeCalledWith(yaml);

      expect(compExec)
        .toBeCalledWith('Broker', 'Byron', 'up');
    });
  });
});
