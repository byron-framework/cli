import { down } from 'src/commands/infrastructure/down';

import ArgumentMissingError from 'src/errors/ArgumentMissingError';
import FileSystemError from 'src/errors/FileSystemError';

describe('component init command', () => {
  const compExec: (
    name: string,
    namespace: string,
    command: string) => void = jest.fn();

  const defaultArgs: string[] = [
    'node',
    'byron',
    'infrastructure',
  ];

  const infrastructureDownArgs: string[] = [
    ...defaultArgs,
    'down',
  ];

  const fswMock: any = {
    instance: {
      checkBuildDir: jest.fn(),
    },
  };

  const infrastructureDownHelpMenu: string = `Infrastructure Down Help Menu`;

  describe('when called with local help flag', () => {
    beforeEach(() => {
      process.argv = [ ...infrastructureDownArgs, '--help' ];
      console.log = jest.fn();
      down();
    });

    it('should display help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(infrastructureDownHelpMenu);
    });
  });

  describe('when called with missing args', () => {
    beforeEach(() => {
      process.argv = [...infrastructureDownArgs];
    });

    it('should throw ArgumentMissingError', () => {
      expect(down)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called with wrong flag', () => {
    beforeEach(() => {
      process.argv = [...infrastructureDownArgs, '-S', 'Byron'];
    });

    it('should throw ArgumentMissingError', () => {
      expect(down)
        .toThrowError(ArgumentMissingError);
    });
  });

  describe('when called with non existent directory', () => {
    beforeEach(() => {
      process.argv = [...infrastructureDownArgs, '-N', 'Byron'];
      fswMock.instance.checkBuildDir = jest
        .fn()
        .mockReturnValueOnce(false);
    });

    it('should throw FileSystemError', () => {
      expect(() => down(fswMock, compExec))
        .toThrowError(FileSystemError);
    });
  });

  describe('when called correctly with short args', () => {
    beforeEach(() => {
      process.argv = [ ...infrastructureDownArgs, '-N', 'Byron' ];
      fswMock.instance.checkBuildDir = jest
        .fn()
        .mockReturnValueOnce(true);
      down(fswMock, compExec);
    });

    it('should call with given namespace', () => {
      expect(fswMock.instance.checkBuildDir)
        .toBeCalledWith('Byron', 'Broker');

      expect(compExec)
        .toBeCalledWith('Broker', 'Byron', 'down');
    });
  });

  describe('when called correctly with long args', () => {
    beforeEach(() => {
      process.argv = [ ...infrastructureDownArgs, '--namespace', 'Byron' ];
      fswMock.instance.checkBuildDir = jest
        .fn()
        .mockReturnValueOnce(true);
      down(fswMock, compExec);
    });

    it('should call with given namespace', () => {
      expect(fswMock.instance.checkBuildDir)
        .toBeCalledWith('Byron', 'Broker');

      expect(compExec)
        .toBeCalledWith('Broker', 'Byron', 'down');
    });
  });
});
