import { view } from 'src/commands/help';

describe('Global Help Menu', () => {
  describe('View', () => {
    let object: any;

    beforeEach(() => {
      object = {
        type: 'option',
        name: 'help',
        description: 'Print this menu and exit',
        formatItem: view.formatItem,
      };
    });

    it('should format the line properly', () => {
      expect(object.formatItem())
        .toBe('--help      Print this menu and exit');
    });
  });
});
