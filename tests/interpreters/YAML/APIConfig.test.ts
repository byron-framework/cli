import interpretAPIConfig from 'src/interpreters/YAML/APIConfig';

import APIConfig from 'src/component/APIConfig';
import Component from 'src/component/Component';
import InnerNode from 'src/component/InnerNode';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLConfig } from 'src/typings/interpreters/YAML/interpreter';

describe('YAML Interpret APIConfig', () => {
  let comp: Component;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
  });

  describe('when schema has a valid APIConfig description', () => {
    beforeEach(() => {
      const config: YAMLConfig = {
        api: {
          port: 3000,
        },
      };

      interpretAPIConfig(config, comp);
    });

    it('should create a APIConfig child', () => {
      const child: InnerNode = comp.children[0] as InnerNode;

      expect(child)
        .toBeInstanceOf(APIConfig);
    });
  });

  describe('when a APIConfig object is invalid', () => {
    let config: any;

    beforeEach(() => {
      comp = new Component('foo', 'bar');
    });

    describe('when there is no api', () => {
      beforeEach(() => {
        config = {};
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretAPIConfig(config, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when api has no port', () => {
      beforeEach(() => {
        config = { api: {} };
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretAPIConfig(config, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when port is not a number', () => {
      beforeEach(() => {
        config = { api: { port: '3000' } };
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretAPIConfig(config, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });
  });
});
