import jsyaml from 'js-yaml';

import FileSystemError from 'src/errors/FileSystemError';
import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { interpret } from 'src/interpreters/YAML/interpreter';

import interpretAPIConfig from 'src/interpreters/YAML/APIConfig';
import interpretCommands from 'src/interpreters/YAML/commands';
import interpretEntities from 'src/interpreters/YAML/entities';
import interpretHandlers from 'src/interpreters/YAML/handlers';
import interpretInputs from 'src/interpreters/YAML/inputs';

import Component from 'src/component/Component';

jest.mock('js-yaml');
jest.mock('src/interpreters/YAML/handlers');
jest.mock('src/interpreters/YAML/commands');
jest.mock('src/interpreters/YAML/entities');
jest.mock('src/interpreters/YAML/inputs');
jest.mock('src/interpreters/YAML/APIConfig');

describe('YAML interpreter', () => {
  describe('when there is no schema inside', () => {
    const fswMock: any = {
      instance: {
        checkSchema: jest.fn(),
      },
    };

    beforeEach(() => {
      fswMock.instance.checkSchema
        .mockReturnValueOnce(false);
    });

    it('should throw a FileSystemError', () => {
      expect(() => interpret(fswMock))
        .toThrow(FileSystemError);
    });
  });

  describe('when there is schema inside', () => {
    const fswMock: any = {
      instance: {
        checkSchema: jest.fn(),
        readSchema: jest.fn(),
      },
    };

    const baseMockSchema: any = {
      name: 'foo',
      namespace: 'boo',
      content: {
        commands: [{
          name: 'fooResolver',
          type: 'query',
          returnType: 'String',
        }],
      },
    };

    describe('when schema is valid', () => {
      beforeEach(() => {
        const localMock: any = Object
          .assign({}, baseMockSchema);

        fswMock.instance.checkSchema
          .mockReturnValueOnce(true);

        jsyaml.safeLoad = jest
          .fn()
          .mockReturnValueOnce(localMock);
      });

      it('should create a Component with given name and namespace', () => {
        const component: Component = interpret(fswMock);
        expect(component)
          .toBeInstanceOf(Component);

        expect(component.name)
          .toBe(baseMockSchema.name);

        expect(component.namespace)
          .toBe(baseMockSchema.namespace);
      });

      describe('when schema contains a Entity description', () => {
        beforeEach(() => {
          const localMock: any = Object
            .assign({}, baseMockSchema);

          localMock.content = {
            types: [{
              name: 'Foo',
              attributes: [
                { name: 'bar', type: 'String!' },
                { name: 'baz', type: '[String!]!' },
              ],
            }],
          };

          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce(localMock);

          interpret(fswMock);
        });

        it('should call interpretEntities', () => {
          expect(interpretEntities)
            .toBeCalled();
        });
      });

      describe('when schema contains a Input description', () => {
        beforeEach(() => {
          const localMock: any = Object
            .assign({}, baseMockSchema);

          localMock.content = {
            inputs: [{
              name: 'Foo',
              attributes: [
                { name: 'bar', type: 'String!' },
                { name: 'baz', type: '[String!]!' },
              ],
            }],
          };

          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce(localMock);

          interpret(fswMock);
        });

        it('should call interpretInputs', () => {
          expect(interpretInputs)
            .toBeCalled();
        });
      });

      describe('when schema contains a Command description', () => {
        beforeEach(() => {
          const localMock: any = Object
            .assign({}, baseMockSchema);

          localMock.content = {
            commands: [{
              name: 'Foo',
              type: 'query',
              returnType: 'String!',
              parameters: [{
                name: 'data',
                type: 'Int!',
              }],
            }],
          };

          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce(localMock);

          interpret(fswMock);
        });

        it('should call interpretCommands', () => {
          expect(interpretCommands)
            .toBeCalled();
        });
      });

      describe('when schema contains a Handler description', () => {
        beforeEach(() => {
          const localMock: any = Object
            .assign({}, baseMockSchema);

          localMock.content = {
            handlers: [{
              name: 'Foo',
              event: 'foo.bar',
            }],
          };

          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce(localMock);

          interpret(fswMock);
        });

        it('call interpretHandlers', () => {
          expect(interpretHandlers)
            .toBeCalled();
        });
      });

      describe('when schema contains a Config description', () => {
        beforeEach(() => {
          const localMock: any = Object
            .assign({}, baseMockSchema);

          localMock.config = {
            api: {
              port: 3000,
            },
          };

          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce(localMock);

          interpret(fswMock);
        });

        it('should call interpretAPIConfig', () => {
          expect(interpretAPIConfig)
            .toBeCalled();
        });
      });

      describe('when schema contains a Hook description', () => {
        describe(`when hook is a pre-command with 'only' filter`, () => {
          beforeEach(() => {
            const localMock: any = Object
              .assign({}, baseMockSchema);

            localMock.content.hooks = [{
              name: 'basicLogger',
              type: 'commandHook',
              pre: {
                only: [
                  'Foo',
                ],
              },
            }];
          });

          it('should not throw SchemaSyntaxError', () => {
            expect(() => interpret(fswMock))
              .not
              .toThrow(SchemaSyntaxError);
          });
        });

        // describe(`when hook is a pre-command with 'except' filter`);
        // describe(`when hook is a pre-command without any filters`);
        // describe(`when hook is a pre-handler with 'only' filter`);
        // describe(`when hook is a pre-handler with 'except' filter`);
        // describe(`when hook is a pre-handler without any filters`);
      });
    });

    describe('when schema is invalid', () => {
      beforeEach(() => {
        fswMock.instance.checkSchema
          .mockReturnValueOnce(true);
      });

      describe('when content is an array', () => {
        beforeEach(() => {
          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce({ content: [] });
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpret(fswMock))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when content is not an object', () => {
        beforeEach(() => {
          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce({ content: 'string' });
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpret(fswMock))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when there is no name, namespace or content', () => {
        beforeEach(() => {
          jsyaml.safeLoad = jest
            .fn()
            .mockReturnValueOnce({ boo: 'foo' });
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpret(fswMock))
            .toThrowError(SchemaSyntaxError);
        });
      });
    });
  });
});
