import interpretHandlers from 'src/interpreters/YAML/handlers';

import InnerNode from 'src/component/InnerNode';

import Component from 'src/component/Component';
import Handler from 'src/component/Handler';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLHandler } from 'src/typings/interpreters/YAML/interpreter';

describe('YAML Interpret Handlers', () => {
  let comp: Component;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
  });

  describe('when schema a valid Handler description', () => {
    beforeEach(() => {
      const handlers: YAMLHandler[] = [{
        name: 'Foo',
        event: 'bar.void',
      }];

      interpretHandlers(handlers, comp);
    });

    it('should create a Handler child', () => {
      const child: InnerNode = comp.children[0] as InnerNode;

      expect(child)
        .toBeInstanceOf(Handler);
    });
  });

  describe('when a Handler object is invalid', () => {
    let handlers: any[];

    describe('when there is no name', () => {
      beforeEach(() => {
        handlers = [{
          event: 'foo.bar',
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretHandlers(handlers, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when there is no event', () => {
      beforeEach(() => {
        handlers = [{
          name: 'Foo',
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretHandlers(handlers, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });
  });
});
