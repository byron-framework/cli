import interpretInputs from 'src/interpreters/YAML/inputs';

import InnerNode from 'src/component/InnerNode';

import Component from 'src/component/Component';
import Field from 'src/component/Field';
import Input from 'src/component/Input';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLEntity } from 'src/typings/interpreters/YAML/interpreter';

describe('YAML Interpret Inputs', () => {
  let comp: Component;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
  });

  describe('when schema a valid Input description', () => {
    beforeEach(() => {
      const inputs: YAMLEntity[] = [{
        name: 'Foo',
        attributes: [
          { name: 'bar', type: 'String!' },
          { name: 'baz', type: '[String!]!' },
        ],
      }];

      interpretInputs(inputs, comp);
    });

    it('should create a Input child', () => {
      const child: InnerNode = comp.children[0] as InnerNode;

      expect(child)
        .toBeInstanceOf(Input);

      expect(child.children[0])
        .toBeInstanceOf(Field);
    });
  });

  describe('when a Input object is invalid', () => {
    let inputs: any[];

    describe('when there is no name', () => {
      beforeEach(() => {
        inputs = [{
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretInputs(inputs, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when attributes are invalid', () => {
      describe('when attributes are null', () => {
        beforeEach(() => {
          inputs = [{
            name: 'Foo',
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretInputs(inputs, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attributes are not an array', () => {
        beforeEach(() => {
          inputs = [{
            name: 'Foo',
            attributes: 'String',
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretInputs(inputs, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attribute have a null name', () => {
        beforeEach(() => {
          inputs = [{
            name: 'Foo',
            attributes: [{ type: 'String' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretInputs(inputs, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attribute have a null type', () => {
        beforeEach(() => {
          inputs = [{
            name: 'Foo',
            attributes: [{ name: 'email' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretInputs(inputs, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });
    });
  });
});
