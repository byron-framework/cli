import interpretEntities from 'src/interpreters/YAML/entities';

import InnerNode from 'src/component/InnerNode';

import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLEntity } from 'src/typings/interpreters/YAML/interpreter';

describe('YAML Interpret Entities', () => {
  let comp: Component;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
  });

  describe('when schema a valid Entity description', () => {
    beforeEach(() => {
      const entities: YAMLEntity[] = [{
        name: 'Foo',
        attributes: [
          { name: 'bar', type: 'String!' },
          { name: 'baz', type: '[String!]!' },
        ],
      }];

      interpretEntities(entities, comp);
    });

    it('should create a Entity child', () => {
      const child: InnerNode = comp.children[0] as InnerNode;

      expect(child)
        .toBeInstanceOf(Entity);

      expect(child.children[0])
        .toBeInstanceOf(Field);
    });
  });

  describe('when a Entity object is invalid', () => {
    let entities: any[];

    describe('when there is no name', () => {
      beforeEach(() => {
        entities = [{
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretEntities(entities, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when attributes are invalid', () => {
      describe('when attributes are null', () => {
        beforeEach(() => {
          entities = [{
            name: 'Foo',
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretEntities(entities, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attributes are not an array', () => {
        beforeEach(() => {
          entities = [{
            name: 'Foo',
            attributes: 'String',
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretEntities(entities, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attribute have a null name', () => {
        beforeEach(() => {
          entities = [{
            name: 'Foo',
            attributes: [{ type: 'String' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretEntities(entities, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when attribute have a null type', () => {
        beforeEach(() => {
          entities = [{
            name: 'Foo',
            attributes: [{ name: 'email' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretEntities(entities, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });
    });
  });
});
