import interpretCommands from 'src/interpreters/YAML/commands';

import InnerNode from 'src/component/InnerNode';

import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Parameter from 'src/component/Parameter';
import ReturnType from 'src/component/ReturnType';

import SchemaSyntaxError from 'src/errors/SchemaSyntaxError';

import { YAMLCommand } from 'src/typings/interpreters/YAML/interpreter';

describe('YAML Interpret Commands', () => {
  let comp: Component;

  describe('when schema a valid Command description', () => {
    let commands: YAMLCommand[];

    beforeEach(() => {
      comp = new Component('foo', 'bar');

      commands = [{
        name: 'Foo',
        type: 'query',
        returnType: 'String!',
        parameters: [{
          name: 'data',
          type: 'Int!',
        }],
      }];

      interpretCommands(commands, comp);
    });

    it('should create a Command child', () => {
      const child: InnerNode = comp.children[0] as InnerNode;

      expect(child)
        .toBeInstanceOf(Command);

      expect(child.children[0])
        .toBeInstanceOf(Parameter);

      expect(child.children[1])
        .toBeInstanceOf(ReturnType);
    });

    describe('when there is no parameters', () => {
      beforeEach(() => {
        comp = new Component('foo', 'bar');

        commands = [{
          name: 'Foo',
          type: 'String!',
          returnType: 'String',
        }];

        interpretCommands(commands, comp);
      });

      it('should throw SchemaSyntaxError', () => {
        const child: InnerNode = comp.children[0] as InnerNode;

        expect(child)
          .toBeInstanceOf(Command);

        expect(child.children[0])
          .toBeInstanceOf(ReturnType);
      });
    });
  });

  describe('when a Command object is invalid', () => {
    let commands: any[];

    describe('when there is no name', () => {
      beforeEach(() => {
        commands = [{
          type: 'Query',
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretCommands(commands, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when there is no type', () => {
      beforeEach(() => {
        commands = [{
          name: 'Foo',
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretCommands(commands, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when there is no returnType', () => {
      beforeEach(() => {
        commands = [{
          name: 'Foo',
          type: 'String!',
        }];
      });

      it('should throw SchemaSyntaxError', () => {
        expect(() => interpretCommands(commands, comp))
          .toThrowError(SchemaSyntaxError);
      });
    });

    describe('when parameters are invalid', () => {
      describe('when paremeters are not an array', () => {
        beforeEach(() => {
          commands = [{
            name: 'Foo',
            type: 'String!',
            returnType: 'String',
            parameters: 'String',
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretCommands(commands, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when paremeter have a null name', () => {
        beforeEach(() => {
          commands = [{
            name: 'Foo',
            type: 'String!',
            returnType: 'String',
            parameters: [{ type: 'String' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretCommands(commands, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

      describe('when paremeter have a null type', () => {
        beforeEach(() => {
          commands = [{
            name: 'Foo',
            type: 'String!',
            returnType: 'String',
            parameters: [{ name: 'email' }],
          }];
        });

        it('should throw SchemaSyntaxError', () => {
          expect(() => interpretCommands(commands, comp))
            .toThrowError(SchemaSyntaxError);
        });
      });

    });
  });
});
