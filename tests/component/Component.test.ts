import Component from 'src/component/Component';

const gen: any = { 
  visitComponent: jest.fn(),
};

describe('Component', () => {
  let comp: Component;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
  });

  it('should create with given arguments', () => {
    expect(comp.name)
      .toEqual('foo');

    expect(comp.namespace)
      .toEqual('bar');
  });

  it('should accpet a generator', () => {
    const child: Component = new Component('bar', 'foo');
    child.accept = jest.fn();
    comp.addChild(child);

    comp.accept(gen);

    expect(gen.visitComponent)
      .toBeCalledWith(comp);

    expect(child.accept)
      .toBeCalledWith(gen);
  });
});
