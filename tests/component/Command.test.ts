import Command from 'src/component/Command';

const gen: any = {
  visitCommand: jest.fn(),
};

describe('Command', () => {
  let command: Command;

  beforeEach(() => {
    command = new Command('createUser', 'quEry');
  });

  it('should create with given arguments', () => {
    expect(command.name)
      .toBe('createUser');
    expect(command.type)
      .toBe('Query');
  });

  it('should accpet a generator', () => {
    const child: Command = new Command('bar', 'mutation');
    child.accept = jest.fn();
    command.addChild(child);

    command.accept(gen);

    expect(gen.visitCommand)
      .toBeCalledWith(command);

    expect(child.accept)
      .toBeCalledWith(gen);
  });
});
