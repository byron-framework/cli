import Handler from 'src/component/Handler';

describe('Handler', () => {
  let handler: Handler;
  const name: string = 'Foo';
  const event: string = 'foo.bar';

  beforeEach(() => {
    handler = new Handler(name, event);
  });

  it('should create with given arguments', () => {
    expect(handler.name)
      .toBe(name);
    expect(handler.event)
      .toBe(event);
  });

  describe('when accept is called', () => {
    const gen: any = {
      visitHandler: jest.fn(),
    };

    beforeEach(() => {
      handler.accept(gen);
    });

    it('should accept generator', () => {
      expect(gen.visitHandler)
        .toBeCalledWith(handler);
    });
  });
});
