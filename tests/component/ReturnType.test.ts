import ReturnType from 'src/component/ReturnType';

import Command from 'src/component/Command';

describe('ReturnType', () => {
  let param: ReturnType;
  const command: Command = new Command('createUser', 'Mutation');

  describe('when accept is called', () => {
    const gen: any = {
      visitReturnType: jest.fn(),
    };

    beforeEach(() => {
      param = new ReturnType('String!', command);
      param.accept(gen);
    });

    it('should accept generator', () => {
      expect(gen.visitReturnType)
        .toBeCalledWith(param);
    });
  });
});
