import Input from 'src/component/Input';

const gen: any = { 
  visitInput: jest.fn(),
};

describe('Input', () => {
  let input: Input;
  const inputName: string = 'User';

  beforeEach(() => {
    input = new Input(inputName);
  });

  it('should build with name', () => {
    expect(input.name)
      .toBe(inputName);
  });

  it('should accept generator', () => {
    const child: Input = new Input('Post');
    child.accept = jest.fn();
    input.addChild(child);
    input.accept(gen);

    expect(gen.visitInput)
      .toBeCalledWith(input);

    expect(child.accept)
      .toBeCalledWith(gen);
  });
});
