import InnerNode from 'src/component/InnerNode';
import Generator from 'src/generators/Generator';

class TestNode extends InnerNode {
  public constructor() {
    super();
  }

  public accept(_: Generator): void {
    return;
  }
}

describe('InnerNode', () => {
  let innerNode: TestNode;
  let childNode: TestNode;

  beforeEach(() => {
    innerNode = new TestNode();
    childNode = new TestNode();
    innerNode.addChild(childNode);
  });

  it('should add children', () => {
    expect(innerNode.children[0])
      .toBe(childNode);
  });
});
