import Entity from 'src/component/Entity';

const gen: any = { 
  visitEntity: jest.fn(),
};

describe('Entity', () => {
  let entity: Entity;
  const entityName: string = 'User';

  beforeEach(() => {
    entity = new Entity(entityName);
  });

  it('should build with name', () => {
    expect(entity.name)
      .toBe(entityName);
  });

  it('should accept generator', () => {
    const child: Entity = new Entity('bar');
    child.accept = jest.fn();
    entity.addChild(child);

    entity.accept(gen);

    expect(gen.visitEntity)
      .toBeCalledWith(entity);

    expect(child.accept)
      .toBeCalledWith(gen);
  });
});
