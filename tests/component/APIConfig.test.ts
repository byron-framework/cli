import APIConfig from 'src/component/APIConfig';

const gen: any = {
  visitAPIConfig: jest.fn(),
};

describe('APIConfig', () => {
  let apiConfig: APIConfig;

  const port: number = 3000;

  describe('When port is given', () => {
    beforeEach(() => {
      apiConfig = new APIConfig(port);
    });

    it('should create with given arguments', () => {
      expect(apiConfig.port)
        .toBe(port);
    });
  });

  describe('When generator visits', () => {
    beforeEach(() => {
      apiConfig = new APIConfig(port);
      apiConfig.accept(gen);
    });

    it('should accept a generator', () => {
      expect(gen.visitAPIConfig)
        .toBeCalledWith(apiConfig);
    });
  });

});
