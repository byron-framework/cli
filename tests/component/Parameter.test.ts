import Parameter from 'src/component/Parameter';

import Command from 'src/component/Command';

describe('Parameter', () => {
  let param: Parameter;
  const command: Command = new Command('createUser', 'Mutation');

  describe('when accept is called', () => {
    const gen: any = {
      visitParameter: jest.fn(),
    };

    beforeEach(() => {
      param = new Parameter('User', 'String!', command);
      param.accept(gen);
    });

    it('should accept generator', () => {
      expect(gen.visitParameter)
        .toBeCalledWith(param);
    });
  });
});
