import Type from 'src/component/Type';

import Entity from 'src/component/Entity';

describe('Type', () => {
  let type: Type;
  const entity: Entity = new Entity('User');

  describe('when accept is called', () => {
    const gen: any = {
      visitType: jest.fn(),
    };

    beforeEach(() => {
      type = new Type('String!', entity);
      type.accept(gen);
    });

    it('should accept generator', () => {
      expect(gen.visitType)
        .toBeCalledWith(type);
    });
  });

  describe('when type has a primitive type', () => {
    beforeEach(() => {
      type = new Type('String!', entity);
    });

    it('should create correctly', () => {
      expect(type.rootType)
        .toBe('String');

      expect(type.nullable)
        .toBeFalsy();

      expect(type.isArray)
        .toBeFalsy();

      expect(type.nullableArray)
        .toBeTruthy();

      expect(type.isForeignKey)
        .toBeFalsy();

      expect(type.parent)
        .toBe(entity);
    });
  });

  describe('when type has a primitive array type', () => {
    beforeEach(() => {
      type = new Type('[String!]!', entity);
    });

    it('should create correctly', () => {
      expect(type.rootType)
        .toBe('String');

      expect(type.nullable)
        .toBeFalsy();

      expect(type.isArray)
        .toBeTruthy();

      expect(type.nullableArray)
        .toBeFalsy();

      expect(type.isForeignKey)
        .toBeFalsy();

      expect(type.parent)
        .toBe(entity);
    });
  });

  describe('when type has a non-primitive type', () => {
    beforeEach(() => {
      type = new Type('Comment', entity);
    });

    it('should create correctly COMMENT', () => {
      expect(type.rootType)
        .toBe('Comment');

      expect(type.nullable)
        .toBeTruthy();

      expect(type.isArray)
        .toBeFalsy();

      expect(type.nullableArray)
        .toBeTruthy();

      expect(type.isForeignKey)
        .toBeTruthy();

      expect(type.parent)
        .toBe(entity);
    });
  });

  describe('when type has a non-primitive array type', () => {
    beforeEach(() => {
      type = new Type('[Post!]!', entity);
    });

    it('should create correctly', () => {
      expect(type.rootType)
        .toBe('Post');

      expect(type.nullable)
        .toBeFalsy();

      expect(type.isArray)
        .toBeTruthy();

      expect(type.nullableArray)
        .toBeFalsy();

      expect(type.isForeignKey)
        .toBeTruthy();

      expect(type.parent)
        .toBe(entity);
    });
  });
});
