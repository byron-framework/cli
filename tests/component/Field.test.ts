import Field from 'src/component/Field';

import Entity from 'src/component/Entity';

describe('Field', () => {
  let field: Field;
  const parent: Entity = new Entity('User');

  describe('when accept is called', () => {
    const gen: any = {
      visitField: jest.fn(),
    };

    beforeEach(() => {
      field = new Field('email', 'String!', parent);
      field.accept(gen);
    });

    it('should accept generator', () => {
      expect(gen.visitField)
        .toBeCalledWith(field);
    });
  });

  describe('when field has a primitive type', () => {
    beforeEach(() => {
      field = new Field('email', 'String!', parent);
    });

    it('should create correctly', () => {
      expect(field.name)
        .toBe('email');

      expect(field.rootType)
        .toBe('String');

      expect(field.nullable)
        .toBeFalsy();

      expect(field.isArray)
        .toBeFalsy();

      expect(field.nullableArray)
        .toBeTruthy();

      expect(field.isForeignKey)
        .toBeFalsy();
    });
  });

  describe('when field has a primitive array type', () => {
    beforeEach(() => {
      field = new Field('email', '[String!]!', parent);
    });

    it('should create correctly', () => {
      expect(field.name)
        .toBe('email');

      expect(field.rootType)
        .toBe('String');

      expect(field.nullable)
        .toBeFalsy();

      expect(field.isArray)
        .toBeTruthy();

      expect(field.nullableArray)
        .toBeFalsy();

      expect(field.isForeignKey)
        .toBeFalsy();
    });
  });

  describe('when field has a non-primitive type', () => {
    beforeEach(() => {
      field = new Field('comment', 'Comment', parent);
    });

    it('should create correctly COMMENT', () => {
      expect(field.name)
        .toBe('comment');

      expect(field.rootType)
        .toBe('Comment');

      expect(field.nullable)
        .toBeTruthy();

      expect(field.isArray)
        .toBeFalsy();

      expect(field.nullableArray)
        .toBeTruthy();

      expect(field.isForeignKey)
        .toBeTruthy();
    });
  });

  describe('when field has a non-primitive array type', () => {
    beforeEach(() => {
      field = new Field('posts', '[Post!]!', parent);
    });

    it('should create correctly', () => {
      expect(field.name)
        .toBe('posts');

      expect(field.rootType)
        .toBe('Post');

      expect(field.nullable)
        .toBeFalsy();

      expect(field.isArray)
        .toBeTruthy();

      expect(field.nullableArray)
        .toBeFalsy();

      expect(field.isForeignKey)
        .toBeTruthy();
    });
  });
});
