import { cli } from 'src/commands/cli';

import UnknownCommandError from 'src/errors/UnknownCommandError';

describe('CLI', () => {
  const helpMenu: string = `Help menu`;
  const globalHelpMock: jest.Mock = jest.fn(() => helpMenu);

  const defaultArgs: string[] = [
    'node',
    'byron',
  ];

  describe('when called without arguments', () => {
    beforeEach(() => {
      process.argv = [...defaultArgs];
      console.log = jest.fn();
      cli(globalHelpMock);
    });

    it('should show help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(helpMenu);
    });
  });

  describe('when called with global help flag', () => {
    beforeEach(() => {
      process.argv = [ ...defaultArgs, '--help' ];
      console.log = jest.fn();
      cli(globalHelpMock);
    });

    it('should show help menu', () => {
      expect(console.log)
        .toHaveBeenCalledWith(helpMenu);
    });
  });

  describe('when called with unknown management command', () => {
    beforeEach(() => {
      process.argv = [
        ...defaultArgs,
        'unknown',
      ];
    });

    it('should throw UnknownCommandError', () => {
      expect(cli)
        .toThrowError(UnknownCommandError);
    });
  });

  describe('when called with known management command', () => {
    beforeEach(() => {
      process.argv = [
        ...defaultArgs,
        'component',
      ];
    });

    it('should execute component command', () => {
      expect(cli)
        .not
        .toThrowError(UnknownCommandError);
    });
  });

  describe('COMPONENT management command', () => {
    const componentArgs: string[] = [
      ...defaultArgs,
      'component',
    ];

    const componentHelpMenu: string = `Component Help Menu`;

    describe('when called with unknown command', () => {
      beforeEach(() => {
        process.argv = [
          ...componentArgs,
          'unknown',
        ];
      });

      it('should throw UnknownCommandError', () => {
        expect(cli)
          .toThrowError(UnknownCommandError);
      });
    });

    describe('when called without any arguments', () => {
      beforeEach(() => {
        process.argv = [ ...componentArgs ];
        console.log = jest.fn();
        cli();
      });

      it('should display its help menu', () => {
        expect(console.log)
          .toHaveBeenCalledWith(componentHelpMenu);
      });
    });
  });

  describe('INFRASTRUCTURE management command', () => {
    const infrastructureArgs: string[] = [
      ...defaultArgs,
      'infrastructure',
    ];

    const infrastructureHelpMenu: string = `Infrastructure Help Menu`;

    describe('when called with unknown command', () => {
      beforeEach(() => {
        process.argv = [
          ...infrastructureArgs,
          'unknown',
        ];
      });

      it('should throw UnknownCommandError', () => {
        expect(cli)
          .toThrowError(UnknownCommandError);
      });
    });

    describe('when called without any arguments', () => {
      beforeEach(() => {
        process.argv = [ ...infrastructureArgs ];
        console.log = jest.fn();
        cli();
      });

      it('should display its help menu', () => {
        expect(console.log)
          .toHaveBeenCalledWith(infrastructureHelpMenu);
      });
    });
  });
});
