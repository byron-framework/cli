import { resolve } from 'path';

// tslint:disable-next-line: no-var-requires
const fs: any = require('fs-extra');

import FSWrapper from 'src/FSWrapper';

jest.mock('fs-extra');

describe('FSWrapper', () => {
  const fsw: FSWrapper = FSWrapper.instance;

  describe('Singleton', () => {
    it('should have a single instance', () => {
      const fs1: FSWrapper = FSWrapper.instance;
      const fs2: FSWrapper = FSWrapper.instance;

      expect(fs1 === fs2)
        .toBe(true);
    });
  });

  describe('checkRootDir', () => {
    describe('when directory does not exist', () => {
      const unexistentDir: string = 'Foo';

      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(false);
      });

      it('should return false', () => {
        expect(fsw.checkRootDir(unexistentDir))
          .toBe(false);
      });
    });

    describe('when directory exists', () => {
      const existentDir: string = 'Foo';

      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(true);
      });

      it('should return true', () => {
        expect(fsw.checkRootDir(existentDir))
          .toBe(true);
      });
    });
  });

  describe('checkSchema', () => {
    beforeEach(() => {
      Object.defineProperty(fsw, 'rootDirPath', { get: (): string => 'Path' });
    });

    describe('when file does not exist', () => {
      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(false);
      });

      it('should return false', () => {
        expect(fsw.checkSchema('Foo'))
          .toBe(false);
      });
    });

    describe('when file exists', () => {
      beforeEach(() => {
        fs.existsSync
          .mockReturnValueOnce(true);
      });

      it('should return true', () => {
        expect(fsw.checkSchema('Foo'))
          .toBe(true);
      });

      it('should set schemaPath properly', () => {
        fsw.checkSchema('Foo');

        const descriptor: PropertyDescriptor | undefined = Object
          .getOwnPropertyDescriptor(fsw, 'schemaPath');

        if (descriptor) {
          expect(descriptor.value)
            .toBe(resolve('Path', 'Foo'));
        }
      });
    });
  });

  describe('readSchema', () => {
    const mockedSchema: string = 'mocked schema';

    beforeEach(() => {
      Object.defineProperty(fsw, 'schemaPath', { get: (): string => 'Path/schema.yaml' });

      fs.readFileSync
        .mockImplementation((_path: string): Buffer => {
          const size: number = Buffer.byteLength(mockedSchema);
          return Buffer.alloc(size, mockedSchema);
        });
    });

    it('should return a string', () => {
      expect(typeof fsw.readSchema())
        .toBe('string');
    });

    it('should return the schema string', () => {
      expect(fsw.readSchema())
        .toEqual(mockedSchema);
    });
  });

  describe('readTemplate', () => {
    const mockedTemplate: string = 'mocked template';

    beforeEach(() => {
      fs.readFileSync
        .mockImplementation((_path: string): Buffer => {
          const size: number = Buffer.byteLength(mockedTemplate);
          return Buffer.alloc(size, mockedTemplate);
        });
    });

    it('should return the template string', () => {
      expect(fsw.readTemplate(['Template']))
        .toEqual(mockedTemplate);
    });
  });
});
