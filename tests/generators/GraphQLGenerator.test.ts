import GraphQLGenerator from 'src/generators/GraphQLGenerator';

import { readFileSync } from 'fs-extra';
import { resolve } from 'path';

import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';
import Input from 'src/component/Input';
import Parameter from 'src/component/Parameter';
import ReturnType from 'src/component/ReturnType';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

const completeSchema: string =
`type User {
  email: String!
  name: String
  posts: [Post]!
}

input createPostInput {
  text: String!
}

type Query {
  getUser: User!
}

type Mutation {
  createPost(data: createPostInput!, owner: User!): Post!
  createUser(data: createUserInput!): User!
}

type Subscription {
  commentSent: Comment!
}

`;

const onlyQuerySchema: string =
`type Query {
  getUser: User!
}

`;

function readTemplate(templatePath: string[]): string {
  const path: string =
    resolve(__dirname, '..', '..', 'templates', ...templatePath);
  return readFileSync(path)
    .toString();
}

describe('GraphQL Schema Generator', () => {
  let gen: GraphQLGenerator;
  let component: Component;
  const fswMock: any = FSWrapper;
  fswMock.readTemplate = readTemplate;

  const userEntity: Entity = new Entity('User');
  userEntity.addChild(new Field('email', 'String!', userEntity));
  userEntity.addChild(new Field('name', 'String', userEntity));
  userEntity.addChild(new Field('posts', '[Post]!', userEntity));

  const createPostInput: Input = new Input('createPostInput');
  createPostInput.addChild(new Field('text', 'String!', createPostInput));

  const getUserCommand: Command = new Command('getUser', 'query');
  getUserCommand.addChild(new ReturnType('User!', getUserCommand));

  const createPostCommand: Command = new Command('createPost', 'mutation');
  createPostCommand.addChild(new Parameter('data', 'createPostInput!', createPostCommand));
  createPostCommand.addChild(new Parameter('owner', 'User!', createPostCommand));
  createPostCommand.addChild(new ReturnType('Post!', createPostCommand));

  const createUserCommand: Command = new Command('createUser', 'mutation');
  createUserCommand.addChild(new Parameter('data', 'createUserInput!', createUserCommand));
  createUserCommand.addChild(new ReturnType('User!', createUserCommand));

  const commentSentCommand: Command = new Command('commentSent', 'subscription');
  commentSentCommand.addChild(new ReturnType('Comment!', commentSentCommand));

  describe('Complete Schema', () => {
    beforeAll(() => {
      component = new Component('UserService', 'JunoWeb');

      component.addChild(userEntity);
      component.addChild(createPostInput);
      component.addChild(getUserCommand);
      component.addChild(createPostCommand);
      component.addChild(createUserCommand);
      component.addChild(commentSentCommand);
    });

    beforeEach(() => {
      gen = new GraphQLGenerator(fswMock);
      component.accept(gen);
    });

    it('should call render correctly', () => {
      expect(fswMock.writeGraphQLSchema)
        .toBeCalledWith(completeSchema);
    });
  });

  describe('Schema with only query', () => {
    beforeAll(() => {
      component = new Component('UserService', 'JunoWeb');

      component.addChild(getUserCommand);
    });

    beforeEach(() => {
      gen = new GraphQLGenerator(fswMock);
      component.accept(gen);
    });

    it('should call render correctly', () => {
      expect(fswMock.writeGraphQLSchema)
        .toBeCalledWith(onlyQuerySchema);
    });
  });
});
