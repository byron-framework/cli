import DockerComposeGenerator from 'src/generators/DockerComposeGenerator';

import { readFileSync } from 'fs-extra';
import { resolve } from 'path';

import APIConfig from 'src/component/APIConfig';
import Component from 'src/component/Component';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

import os from 'os';
jest.mock('os');

const yaml: string =
`version: '3.5'

services:
  cache_user:
    image: jooaodanieel/mongo-enterprise:4.0
    command: --storageEngine inMemory --dbpath /data/db
    networks:
      - junoweb_services

  api_user:
    build:
      context: ./api
      args:
        port: 3000
        cache_url: 'mongodb://cache_user:27017/test'
        broker_url: 'nats://broker:4222'
        broker_cluster: 'test-cluster'
        broker_client_id: 'user_api'
    environment:
      PORT: 3000
    ports:
      - 3000:3000
    volumes:
      - './api/src:/usr/src/app/src'
      - 'api_dependencies:/usr/src/app/node_modules'
    networks:
      - junoweb_services
    depends_on:
      - cache_user

  sink_user:
    build:
      context: ./sink
      args:
        cache_url: 'mongodb://cache_user:27017/test'
        broker_url: 'nats://broker:4222'
        broker_cluster: 'test-cluster'
        broker_client_id: 'user_sink'
    volumes:
      - './sink/src:/usr/src/app/src'
      - 'sink_dependencies:/usr/src/app/node_modules'
    networks:
      - junoweb_services
    depends_on:
      - cache_user

volumes:
  api_dependencies:
  sink_dependencies:

networks:
  junoweb_services:
    name: junoweb_mesh`;

function readTemplate(templatePath: string[]): string {
  const path: string =
    resolve(__dirname, '..', '..', 'templates', ...templatePath);
  return readFileSync(path)
    .toString();
}

describe('DockerComposeGenerator', () => {
  let gen: DockerComposeGenerator;
  let component: Component;

  const fswMock: any = FSWrapper;
  fswMock.readTemplate = readTemplate;

  const apiConfig: APIConfig = new APIConfig(3000);

  beforeAll(() => {
    component = new Component('user', 'junoweb');
    component.addChild(apiConfig);
  });

  beforeEach(() => {
    os.userInfo = jest
      .fn()
      .mockReturnValue({
        uid: 1000,
        gid: 1000,
        shell: '/usr/sh',
        homedir: '/home/test',
        username: 'test',
      });
    gen = new DockerComposeGenerator(fswMock);
    component.accept(gen);
  });

  it('should call render correctly', () => {
    expect(fswMock.writeDockerCompose)
      .toBeCalledWith(yaml);
  });
});
