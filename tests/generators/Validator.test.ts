import Validator from 'src/generators/Validator';

import ValidationError from 'src/errors/ValidationError';

import Command from 'src/component/Command';
import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';
import Handler from 'src/component/Handler';
import Hook from 'src/component/Hook';
import Input from 'src/component/Input';

describe('Validation of', () => {
  describe('Entities', () => {
    const validator: Validator = new Validator();

    describe('name', () => {
      beforeEach(() => {
        validator.visitField = jest
          .fn()
          .mockImplementationOnce(() => {
            return;
          });
      });

      describe('when it has none', () => {
        let entity: Entity;
        beforeEach(() => {
          entity = new Entity('');
        });

        it('should throw a Validation Error', () => {
          expect(() => entity.accept(validator))
            .toThrow(ValidationError);
        });
      });

      describe('when it is not unique', () => {
        let one: Entity;
        let two: Entity;
        beforeEach(() => {
          const commonName: string = 'SameEntityName';

          one = new Entity(commonName);
          one.addChild(new Field('_id', 'ID!', one));

          two = new Entity(commonName);

          one.accept(validator);
        });

        it('should throw a Validation Error', () => {
          expect(() => two.accept(validator))
            .toThrow(ValidationError);
        });
      });
    });

    describe('when it has not Fields', () => {
      let entity: Entity;

      beforeEach(() => {
        entity = new Entity('name');
      });

      it('should throw a Validation Error', () => {
        expect(() => entity.accept(validator))
          .toThrow(ValidationError);
      });
    });
  });

  describe('Inputs', () => {
    const validator: Validator = new Validator();

    describe('name', () => {
      beforeEach(() => {
        validator.visitField = jest
          .fn()
          .mockImplementationOnce(() => {
            return;
          });
      });

      describe('when it has none', () => {
        let input: Input;
        beforeEach(() => {
          input = new Input('');
        });

        it('should throw a Validation Error', () => {
          expect(() => input.accept(validator))
            .toThrow(ValidationError);
        });
      });

      describe('when it is not unique', () => {
        let one: Input;
        let two: Input;
        beforeEach(() => {
          const commonName: string = 'SameInputName';

          one = new Input(commonName);
          one.addChild(new Field('_id', 'ID!', one));

          two = new Input(commonName);

          one.accept(validator);
        });

        it('should throw a Validation Error', () => {
          expect(() => two.accept(validator))
            .toThrow(ValidationError);
        });
      });
    });

    describe('when it has not Fields', () => {
      let input: Input;

      beforeEach(() => {
        input = new Input('name');
      });

      it('should throw a Validation Error', () => {
        expect(() => input.accept(validator))
          .toThrow(ValidationError);
      });
    });

  });

  describe('Fields', () => {
    const validator: Validator = new Validator();

    describe('name', () => {
      describe('when it has none', () => {
        const parent: Entity = new Entity('Parent');
        let field: Field;

        beforeEach(() => {
          field = new Field('', 'String', parent);
        });

        it('should throw a Validation Error', () => {
          expect(() => field.accept(validator))
            .toThrow(ValidationError);
        });
      });

      describe('when it is not unique', () => {
        const parent: Entity = new Entity('Parent');

        let firstField: Field;
        let secondField: Field;

        beforeEach(() => {
          const commonName: string = 'SameFieldName';

          firstField = new Field(commonName, 'String', parent);
          firstField.accept(validator);

          secondField = new Field(commonName, 'Int', parent);
        });

        it('should throw a Validation Error', () => {
          expect(() => secondField.accept(validator))
            .toThrow(ValidationError);
        });
      });
    });

    describe('when root type is not known', () => {
      const otherValidator: Validator = new Validator();
      let component: Component;

      beforeEach(() => {
        const entity: Entity = new Entity('Anything');
        entity.addChild(new Field('_id', 'ID!', entity));
        entity.addChild(new Field('something', 'Something!', entity));

        component = new Component('name', 'namespace');
        component.addChild(entity);
        component.addChild(new Command('blaus', 'Query'));
      });

      it('should throw a Validation Error', () => {
        expect(() => component.accept(otherValidator))
          .toThrow(/Unknown/i);
      });
    });
  });

  describe('Commands', () => {
    const validator: Validator = new Validator();

    describe('name', () => {
      beforeEach(() => {
        validator.visitParameter = jest
          .fn()
          .mockImplementationOnce(() => {
            return;
          });
      });

      describe('when it has none', () => {
        let command: Command;

        beforeEach(() => {
          command = new Command('', 'query');
        });

        it('should throw a Validation Error', () => {
          expect(() => command.accept(validator))
            .toThrow(ValidationError);
        });
      });

      describe('when it is not unique', () => {
        let firstComand: Command;
        let secondComand: Command;

        beforeEach(() => {
          const commonName: string = 'SameCommandName';

          firstComand = new Command(commonName, 'query');
          firstComand.accept(validator);

          secondComand = new Command(commonName, '');
        });

        it('should throw a Validation Error', () => {
          expect(() => secondComand.accept(validator))
            .toThrow(ValidationError);
        });
      });
    });

    describe('when type is not [Query, Mutation, Subscription]', () => {
      let command: Command;

      beforeEach(() => {
        command = new Command('Name', 'foobar');
      });

      it('should throw a Validation Error', () => {
        expect(() => command.accept(validator))
          .toThrow(ValidationError);
      });
    });

    describe('when there is no Query', () => {
      const newValidator: Validator = new Validator();
      let component: Component;

      beforeEach(() => {
        component = new Component('name', 'namespace');
        component.addChild(new Command('foo', 'Mutation'));
      });

      it('should throw a Validation Error', () => {
        expect(() => component.accept(newValidator))
          .toThrow(ValidationError);
      });
    });
  });

  describe('Handler', () => {
    const validator: Validator = new Validator();

    describe('when it is not unique', () => {
      let firstComand: Handler;
      let secondComand: Handler;

      beforeEach(() => {
        const commonName: string = 'SameHandlerName';

        firstComand = new Handler(commonName, 'foo.bar');
        firstComand.accept(validator);

        secondComand = new Handler(commonName, 'foo.bar');
      });

      it('should throw a Validation Error', () => {
        expect(() => secondComand.accept(validator))
          .toThrow(ValidationError);
      });
    });
  });

  describe('Hooks', () => {
    const component: Component = new Component('name', 'namespace');
    const validator: Validator = new Validator();

    beforeEach(() => {
      component.addChild(new Hook('name', 'foo', { pre: {}}));

      Object.defineProperty(validator, 'queriesCount', { get: (): number => 1 });
    });

    it('should throw a Validation Error', () => {
      expect(() => component.accept(validator))
        .toThrow(ValidationError);
    });
  });
});
