import MongoModelGenerator from 'src/generators/MongoModelGenerator';

import { readFileSync } from 'fs-extra';
import { resolve } from 'path';

import Component from 'src/component/Component';
import Entity from 'src/component/Entity';
import Field from 'src/component/Field';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

function readTemplate(templatePath: string[]): string {
  const path: string =
    resolve(__dirname, '..', '..', 'templates', ...templatePath);
  return readFileSync(path)
    .toString();
}

const UserSchema: string =
`import { model, Schema } from 'mongoose';

const User: any = model<any>('User', new Schema({
  email: String,
  name: String,
  posts: [{ type: String, ref: 'Post' }],
}));

export default User;
`;

const PostSchema: string =
`import { model, Schema } from 'mongoose';

const Post: any = model<any>('Post', new Schema({
  content: String,
}));

export default Post;
`;

describe('Mongo Model Generator', () => {
  let gen: MongoModelGenerator;
  let component: Component;
  const fswMock: any = FSWrapper;
  fswMock.readTemplate = readTemplate;

  const userEntityName: string = 'User';
  const userEntity: Entity = new Entity(userEntityName);
  userEntity.addChild(new Field('email', 'String!', userEntity));
  userEntity.addChild(new Field('name', 'String', userEntity));
  userEntity.addChild(new Field('posts', '[Post]!', userEntity));

  const postEntityName: string = 'Post';
  const postEntity: Entity = new Entity(postEntityName);
  postEntity.addChild(new Field('content', 'String!', postEntity));

  gen = new MongoModelGenerator(fswMock);

  describe('when there is no foreign key', () => {
    beforeEach(() => {
      component = new Component('UserService', 'JunoWeb');
      component.addChild(postEntity);
      component.accept(gen);
    });

    it('should cll render correctly', () => {
      expect(fswMock.writeMongoModel)
        .toBeCalledWith(postEntityName, PostSchema);
    });
  });

  describe('Name of the group', () => {
    beforeEach(() => {
      component = new Component('UserService', 'JunoWeb');
      component.addChild(userEntity);
      component.accept(gen);
    });

    it('should call render correctly', () => {
      expect(fswMock.writeMongoModel)
        .toBeCalledWith(userEntityName, UserSchema);
    });
  });
});
