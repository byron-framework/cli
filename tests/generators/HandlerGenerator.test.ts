import HandlerGenerator from 'src/generators/HandlerGenerator';

import { readFileSync } from 'fs-extra';
import { resolve } from 'path';

import Component from 'src/component/Component';
import Handler from 'src/component/Handler';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

const eventHandler: string =
`import Event from 'src/Event';
import foo from './devfoo';
import { getPostHooks, getPreHooks } from '../hooks/factory';

export default (ctx: any) => {
  Event.listen('foo.bar', msg => {
    getPreHooks('foo')(ctx, msg);
    foo(ctx, msg);
    getPostHooks('foo')(ctx, msg);
  });
};
`;

function readTemplate(templatePath: string[]): string {
  const path: string =
    resolve(__dirname, '..', '..', 'templates', ...templatePath);
  return readFileSync(path)
    .toString();
}

describe('HandlerGenerator', () => {
  let gen: HandlerGenerator;
  let comp: Component;

  const fswMock: any = FSWrapper;
  fswMock.readTemplate = readTemplate;

  beforeEach(() => {
    comp = new Component('foo', 'bar');
    comp.addChild(new Handler('foo', 'foo.bar'));

    gen = new HandlerGenerator(fswMock);
    comp.accept(gen);
  });

  it('should call copy correctly', () => {
    expect(fswMock.copyHandler)
      .toBeCalledWith('foo');

    expect(fswMock.writeHandler)
      .toBeCalledWith('foo', eventHandler);
  });
});
