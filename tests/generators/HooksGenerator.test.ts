import HooksGenerator from 'src/generators/HooksGenerator';

import Component from 'src/component/Component';
import Hook from 'src/component/Hook';

import FSWrapper from 'src/FSWrapper';
jest.mock('src/FSWrapper');

describe('Hook Generator', () => {
  describe('copying a hook', () => {
    const hookName: string = 'name';
    const hook: Hook = new Hook(hookName, 'commandHook', { pre: {} });
    const fs: any = FSWrapper;

    beforeEach(() => {
      const gen: HooksGenerator = new HooksGenerator(fs);
      hook.accept(gen);
    });

    it('should call FSWrapper with the hooks name', () => {
      expect(fs.copyCommandHook)
        .toHaveBeenCalledWith(hookName);
    });
  });

  describe('aggregating of all hooks', () => {
    const fs: any = FSWrapper;
    let commandAggregation: string;
    let handlerAggregation: string;

    beforeEach(() => {
      const gen: HooksGenerator = new HooksGenerator(fs);
      const component: Component = new Component('name', 'namespace');
      const hooks: Hook[] = [
        new Hook('one', 'commandHook', { pre: {} }),
        new Hook('two', 'commandHook', { pre: { only: ['commandOne'] } }),
        new Hook('three', 'commandHook', { pre: { except: ['commandOne'] } }),
        new Hook('four', 'commandHook', { post: {} }),
        new Hook('five', 'commandHook', { post: { only: ['commandOne'] } }),
        new Hook('six', 'commandHook', { post: { except: ['commandOne'] } }),
        new Hook('twenty_one', 'handlerHook', { pre: {} }),
        new Hook('twenty_two', 'handlerHook', { pre: { only: ['handlerOne'] } }),
        new Hook('twenty_three', 'handlerHook', { pre: { except: ['handlerOne'] } }),
        new Hook('twenty_four', 'handlerHook', { post: {} }),
        new Hook('twenty_five', 'handlerHook', { post: { only: ['handlerOne'] } }),
        new Hook('twenty_six', 'handlerHook', { post: { except: ['handlerOne'] } }),
      ];

      commandAggregation = JSON.stringify({
        one: {
          pre: {},
        },
        two: {
          pre: {
            only: ['commandOne'],
          },
        },
        three: {
          pre: {
            except: ['commandOne'],
          },
        },
        four: {
          post: {},
        },
        five: {
          post: {
            only: ['commandOne'],
          },
        },
        six: {
          post: {
            except: ['commandOne'],
          },
        },
      });

      handlerAggregation = JSON.stringify({
        twenty_one: {
          pre: {},
        },
        twenty_two: {
          pre: {
            only: ['handlerOne'],
          },
        },
        twenty_three: {
          pre: {
            except: ['handlerOne'],
          },
        },
        twenty_four: {
          post: {},
        },
        twenty_five: {
          post: {
            only: ['handlerOne'],
          },
        },
        twenty_six: {
          post: {
            except: ['handlerOne'],
          },
        },
      });

      hooks.forEach((hook: Hook): void => component.addChild(hook));
      component.accept(gen);
    });

    it('should write command and handler hook aggregation files', () => {
      expect(fs.writeCommandHooksAggregate)
        .toHaveBeenCalledWith(commandAggregation);

      expect(fs.writeHandlerHooksAggregate)
        .toHaveBeenCalledWith(handlerAggregation);
    });
  });
});
