import { resolve } from 'path';

import filterJSON from './filter.json';

interface Filter {
  [hookName: string]: {
    except?: string[];
    only?: string[];
    function: any;
  };
}

const filters: Filter = filterJSON;

declare type handlerHookFunction = (context: any, msg: any) => void;

Object
  .keys(filters)
  .forEach(async (hookName: string): Promise<void> => {
    const path: string = resolve(__dirname, hookName);
    filters[hookName].function = (await import(path)).default;
  });

export const getPreHooks: (handlerName: string) => any = (handlerName: string): any => {
  const handlerHooks: handlerHookFunction[] = Object
    .keys(filters)
    .filter((hookName: string): boolean => {
      const { except, only }: any = filters[hookName];

      if (only !== undefined) {
        return only.includes(handlerName);
      }

      if (except !== undefined) {
        return !except.includes(handlerName);
      }

      return true;
    })
    .map((hookName: string): any => filters[hookName].function );

  return async (context: any, msg: any): Promise<void> => {
    handlerHooks
      .forEach(async (hook: handlerHookFunction): Promise<void> => {
        await hook(context, msg);
      });
  };
};

export const getPostHooks: (handlerName: string) => handlerHookFunction =
  (handlerName: string): handlerHookFunction => {
    return (context: any, msg: any): any => {
      return { handlerName, context, msg };
    };
  };
