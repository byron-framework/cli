import fs from 'fs';

import { Subscription } from 'node-nats-streaming';

import db, { DbConnection } from 'src/db/models';
import Event, { IEvent, stan } from 'src/Event';
import logger, { Logger } from 'src/logger';

export interface StanContext {
  subs: { [key: string]: Subscription };
  db: DbConnection;
  Event: IEvent;
  logger: Logger;
}

const stanContext: StanContext = {
  subs: {},
  db,
  Event,
  logger,
};

const handlers: any[] = [];

fs.readdirSync('./src/handlers')
  .filter((filename: string): boolean => filename.slice(0, 3) !== 'dev')
  .forEach(async (fileName: string): Promise<void> => {
    const name: string = fileName.slice(0, fileName.length - 3);
    const handler: any = await import(`${__dirname}/handlers/${name}`);
    handlers.push(handler.default);
  });

function gracefulExit(): void {
  process.on('SIGINT', () => {
    console.log('Received SIGINT. Press Control-D to exit.');
  });

  const handle: any = (signal: any): void => {
    console.log(`Received ${signal}`);
    Event.emit('log', `Sink stopping: ${process.env.BROKER_CLIENT_ID}`);
  };

  process.on('SIGINT', handle);
  process.on('SIGTERM', handle);
}

stan.on('connect', (): void => {
  console.log('Sink running!');
  gracefulExit();
  handlers.forEach((handler: any): void => handler(stanContext));

  Event.emit('log', `Sink running: ${process.env.BROKER_CLIENT_ID}`);
});
