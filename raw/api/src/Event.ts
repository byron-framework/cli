import { connect, Stan } from 'node-nats-streaming';

const brokerUrl: string = process.env.BROKER_URL
  ? process.env.BROKER_URL
  : 'nats://broker:4222';

const brokerCluster: string = process.env.BROKER_CLUSTER
  ? process.env.BROKER_CLUSTER
  : 'test-cluster';

const brokerClientId: string = process.env.BROKER_CLIENT_ID
  ? process.env.BROKER_CLIENT_ID
  : 'api';

const stan: Stan = connect(brokerCluster, brokerClientId, {
  url: brokerUrl,
});

const replayAll: any = stan
  .subscriptionOptions()
  .setDeliverAllAvailable();

const unsubscribe: () => void = () => {
  console.log('unsubscribed');
}

interface SubscriptionHash {
  [key: string]: any;
}

const subscriptions: SubscriptionHash = {};

function emit(topic: string, payload: any): void {
  stan.publish(topic, JSON.stringify(payload));
}

async function listen(topic: string, callback: (msg: any) => void, options: any = replayAll) {
  const subs: any = await stan.subscribe(topic, options);
  subs.on('message', callback);
  subscriptions[topic] = subs;
}

async function stop(topic: string, callback: () => void = unsubscribe) {
  const subs: any = subscriptions[topic];
  subs.on('unsubscribed', callback);
  subs.unsubscribe();
}

export interface IEvent {
  emit:   (topic: string, payload: any) => void;
  listen: (topic: string, callback: (msg: any) => void, options?: any) => void;
  stop:   (topic: string, callback: () => void) => void;
}

export default {
  emit,
  listen,
  stop,
};
