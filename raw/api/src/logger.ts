import winston, { Logger } from "winston";

const logger: Logger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: "combined.log" }),
  ],
});

export default logger;

export {
  Logger,
};
