import { resolve } from 'path';

import filterJSON from './filter.json';

interface Filter {
  [hookName: string]: {
    except?: string[];
    only?: string[];
    function: any;
  };
}

const filters: Filter = filterJSON;

declare type commandHookFunction = (parent: any, args: any, context: any, info: any) => void;

Object
  .keys(filters)
  .forEach(async (hookName: string): Promise<void> => {
    const path: string = resolve(__dirname, hookName);
    filters[hookName].function = (await import(path)).default;
  });

export const getPreHooks: (commandName: string) => any = (commandName: string): any => {
  const commandHooks: commandHookFunction[] = Object
    .keys(filters)
    .filter((hookName: string): boolean => {
      const { except, only }: any = filters[hookName];

      if (only !== undefined) {
        return only.includes(commandName);
      }

      if (except !== undefined) {
        return !except.includes(commandName);
      }

      return true;
    })
    .map((hookName: string): any => filters[hookName].function );

  return async (parent: any, args: any, context: any, info: any): Promise<void> => {
    commandHooks
      .forEach(async (hook: commandHookFunction): Promise<void> => {
        await hook(parent, args, context, info);
      });
  };
};

export const getPostHooks: (commandName: string) => any = (commandName: string): any => {
  return (ctx: any): any => {
    return { ctx, commandName };
  };
};
