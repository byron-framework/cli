# Schema API reference

A guide to use our schema API

## Global
Global fields, that stay in the root of the compnent's schema.

### name
**Description**: Name of the component

**Type**: `string`

**Default**: `""`

### namespace
**Description**: Name of the system where the component belongs

**Type**: `string`

**Default**: `""`

### schema
**Description**: Schema of the component

**Type**: `string`

**Default**: `""`

### config
**Description**: Component's configuration

**Type**: `string`

**Default**: `""`


## Schema
Schema fields, the schema builds the logic of the component. 

### ObjectTypes
**Description**: List of component's ObjectTypes

**Type**: `ObjectType[]`

**Default**: `[]`

### Resolvers
**Description**: List of component's Resolvers

**Type**: `Resolvers[]`

**DefaultValue**: `[]`

## ObjectType
Declare a [GraphQL ObjectType](https://graphql.org/learn/schema/)

### name
**Description**: Name of the type

**Type**: `string`

**Default**: `""`

### attributes
**Description**: List of component's Attributes.

**Type**: `Attributes`

**Default**: `[]`

### actions
**Description**: Default actions to be generated, this field is composed by a list of letters from the CRUD
acronym. Adding the `c` generates the `create<Type>` resolver, `new<User>` event handler.

**Type**: `Enum[]`

**Enum values**: `c`, `r`, `u`, `d`

**Default**: `[]`

**Example**: `['c', 'r']`


## Attribute
Declare an attribute of an ObjectType.

### name
**Description**: Name of the Attribute

**Type**: `string`

**Default**: `""`


### type
**Description**: Type of the Attribute. This field accept declared ObjectTypes or default GraphQL types

**Type**: `string`

**Default**: `""` 

## Resolver
Declare a GraphQL resolver, the implementation must be located at the `resolvers` directory, also, the file must have
the same name as the one in the `Resolver` declaration.

### name
**Description**: Name of the Resolver. Must be the same name as the file located at `resolvers/`.

**Type**: `string`

**Default**: `""` 

### type
**Description**: Type of the Resolver.

**Type**: `Enum`

**Enum values**: `mutation, query`

**Default**: `""` 

### args
**Description**: List of the args accepted by the resolver.

**Type**: `Argument[]`

**Default**: `[]` 

### return
**Description**: Type of the resolver return. This field accept declared ObjectTypes or default GraphQL types

**Type**: `string`

**Default**: `""`

## Argument
Declare an resolver's Argument.

### name
**Description**: Name of the Argument

**Type**: `string`

**Default**: `""`

### type
**Description**: Type of the Attribute. This field accept declared ObjectTypes or default GraphQL types

**Type**: `string`

**Default**: `""` 
