# CLI reference

A guide to use our CLI

## Init
Initialize a component boilerplate with a given `name` into a given `path`

### Arguments
1. `name`: name of the generated component. This will be used for the name of the folder. 
If the given name is `User`, the generated directory will be named: `UserComponent`
2. `path`: path where the boilerplate will be generated. If none is provided, the path will be `.`

## Build
Build a given component. The result will be located at `/tmp/<namespace>/<name>`. The name and 
namespace are specified in the `schema.yml` .

### Arguments
1. `path`: path where the component is located.
