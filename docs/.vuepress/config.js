module.exports = {
  title: 'Byron',
  description: 'Event-Driven Microservices Framework',
  head: [
    ['link', { rel: 'icon', href: '/images/logo/purple-512x512.png' }],
    ['link', { rel: 'manifest', href: '/manifest.json' }],
    ['meta', { name: 'theme-color', content: '#700D89' }]
  ],
  themeConfig: {
    logo: '/images/logo/purple-512x512.png',
    repo: 'https://gitlab.com/byron-framework/cli',
    sidebar: {
      '/guide/': getGuideSidebar('Guide'),
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'CLI', link: '/cli/' },
      { text: 'Schema API', link: '/api/' },
    ]
  },
  plugins: [
    ['@vuepress/pwa', {
      serviceWorker: true,
      updatePopup: true
    }],
  ]
};

function getGuideSidebar(name) {
  return [
    {
      title: name,
      collapsable: false,
      sidebarDepth: 2,
      children: [
        '',
        'component',
        'command',
        'type-input',
        'mutations',
        'handler',
      ],
    },
  ];
}