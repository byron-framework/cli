---
home: true
heroImage: /images/logo/purple-512x512.png
actionText: Get Started →
actionLink: /guide/
features:
- title: Simple To Use
  details: Avoid time consuming boilerplating and dive into coding right away with only one command.
- title: Declarative
  details: Every action a service do is declared in a single file, making it easier to know what actions it's able to perform.
- title: Event Driven
  details: Communication between services is based on asyncronous message exchaging.
---
