---
prev: ./type-input.md
next: ./handler.md
---

# Mutations
Mutations are requests that can generate changes into the data of the system. They
usually change the data by producing events consumed by the handlers across the system,
we will talk more about handlers in the next page. For this, page we will create
a mutation used the model created by the `User` type we created.

## Declaring a mutation
We will add a mutation to create an `User`, firstly add it to schema:

```yml
# schema.yml

name: User
namespace: Byron

content:
  types:
  - name: User
    attributes:
    - name: email
      type: String!
    - name: name
      type: String!
    - name: postCount
      type: Int!
  inputs:
  - name: UserInput
    attributes:
    - name: email
      type: String!
    - name: firstName
      type: String!
    - name: lastName
      type: String!
  commands:
  - name: createUser
    type: mutation
    returnType: User!
    parameters:
    - name: data
      type: UserInput!
    ...
```

:::danger
The types of a mutation's parameter can only be a primitive type or a declared input
:::

## Implementing a mutation

Every declared `Type` has its model inserted into `ctx.db`.

```typescript
// api/commands/createUser.ts

const createUser = (_p: any, args: any, ctx: any, _i: any) => {
  const { db: { User }, Event, uuidv4 } = ctx;

  const user = new User({
    _id: uuiv4(),
    email: args.email,
    name: `${args.firstName} ${args.lastName}`
  });

  Event.emit('new.user', user);

  return user;
}

export default createUser;
```

This code will create a new object `User`, emit an event named `new.user` with the created
object as its payload and return this object to the request sender. This event will be consumed
by the handler we will write in the next page.

:::warning
Just creating a object with the model constructor **will not** save it into the cache.
:::

## Executing a mutation

We will do the same process as before, build the component and then run it:

```
byron component down User
byron component build User
byron component up User
```

Now in the playground, we write our mutation:

```graphql
mutation register {
  createUser(data: {
    email: "john.doe@email.com"
    firstName: "John"
    lastName: "Doe"
  }) {
    _id
    name
    email
  }
}
```

Note that, this time the graphql mutation has second part -- the second set of curly braces, this
part tells the API which fields of the `User` type we want returned. The response for this
mutation should be:

```json
{
  "data": {
    "register": {
      "_id": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "name": "John Doe",
      "email": "john.doe@email.com"
    }
  }
}
```