---
next: ./component.md
---

# Introduction

Byron is a **component-based framework** for build systems using a microservice architecture.
The focus of these components are serving performatic and decoupled services.

## Pre-requisites

Make sure you have `Docker` and `docker-compose` installed. If you don't, here are the links:

- Docker [installation guide](https://docs.docker.com/install/)
- docker-compose [installation guide](https://docs.docker.com/compose/install/)

## Installation

Let's install `Byron` CLI:

```bash
$ npm install --global @byronframework/cli
# or
$ yarn global add @byronframework/cli
```
