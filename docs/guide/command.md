---
prev: ./component.md
next: ./type-input.md
---

# Command
As we previously explained, commands are the building block for the API part of a component.
They are functions that respond to a API request and they take four arguments, 
`(parent, args, ctx, info)`: 

::: tip
If you ever worked with GraphQL, Byron's commands are the same as GraphQL resolvers.
:::

## Declaring a command
The first step is to declare it into the `schema.yml`, a command declaration takes four arguments:

1. `name`: It will be the name that shows up into the GraphQL schema, and
the name of the file inside `api/commands` that implements the functionality of this command.
2. `type`: It tells Byron which type a command is:
    * `query`: don't produce any changes into the *cache*
    * `mutation`: produce changes into the *cache*
    * `subscription`: streams data from the API
3. `returnType`: It describes the type of the object return by the command. This field accept the
primitive GraphQL types (`String`, `Int`, `Float`, `ID`) and types declared in the schema 
(we'll talk more about types in a next chapter).
4. `parameters` (optional): A list of parameters received by the command. Every `parameter` is
a object composed by a `name` and a `type` (this accept the same values as `returnType`).

:::tip
If you insert a `!` at the end of a type declaration, it means that that type cannot be `null` 
or `undefined`.
:::

```yml
# schema.yml

name: User
namespace: Byron

content:
  commands:
  - name: sayHello
    type: query
    returnType: String
    parameters:
    - name: firstName
      type: String
```

## Implementing a command

The second step is to implement the code to respond this `query`, for this, create the file 
`api/commands/sayHello.ts`.

1. `parent`: This object will contain the return object of a parent command, this enables
you to query nested values.
2. `args`: This object is the values passed as parameters to your command.
3. `ctx`: This is global context of your API, in here you will find the cache connection
and the functions to publish events.
4. `info`: This contains meta-information about the request and API.


```typescript
// api/commands/sayHello.ts

const sayHello = (_p: any, args, _ctx: any, _i: any) => {
  return `Hello ${args.firstName}`;
}

export default sayHello;
```

:::warning
Each component must have at least one query, otherwise it won't build.
:::

## Executing a command
We need to build the component first, we can do this by using `byron component build`,
this command receives the path of the directory that contains the `schema.yml`.

```bash
byron component build User
```

Before running our component, we need to run the `broker`, so it can connect to it.
We can achieve this by running `byron insfrastructure up -N`, passing the namespace
as the argument for the `-N` flag.

```bash
byron infrastruture up -N Byron
```

Now, we can run our component by using the command:
```
byron component up User
```

:::tip
The up command receives the same argument as the build command
:::

After this message appears in the terminal:

```bash
🚀 Server ready at http://localhost:4000/graphql
```

Go to `http://localhost:4000/graphql` in your browser, and the GraphQL Playground should appear. 
On the right pane, we can write our query:

```graphql
query helloJohn {
  sayHello(firstName: "John Doe")
}
```

In the right pane, it will appear, after you press the `play` button, a json as a response:

```json
{
  "data": {
    "sayHello": "Hello John Doe"
  }
}
```

:::tip
You can terminate the component by hitting `CTRL-C` in the terminal. After terminating
the component, run `byron component down User` to delete the containers and its volumes
:::
