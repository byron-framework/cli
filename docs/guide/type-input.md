---
prev: ./command.md
next: ./mutations.md
---

# Types and Input
**Types** are used to generate data models to be saved into the *cache* and new types for
the commands to return. Both *API* and *sink* use the models to read and manipulate the
data inside the *cache*.

**Inputs** are only used by the API, they define new types received by a **mutation command**
as a parameter.

Both types and inputs only require to be declared.

## Declaring a type
A type declaration takes two arguments:

1. `name`: the name of the type.
2. `attributes`: This field is a list of objects composed by a `name` and a `type`. The `type`
field accepts primitive GraphQL types (`String`, `Int`, `Float`, `ID`) and other types 
declared in the schema.

```yml
# schema.yml

name: User
namespace: Byron

content:
  types:
  - name: User
    attributes:
    - name: email
      type: String!
    - name: name
      type: String!
    - name: postCount
      type: Int!
  commands:
    ...
```

## Declaring a input

Inputs take the same arguments as the **type** declaration.

```yml
# schema.yml

name: User
namespace: Byron

content:
  types:
  - name: User
    attributes:
    - name: email
      type: String!
    - name: name
      type: String!
    - name: postCount
      type: Int!
  inputs:
  - name: UserInput
    attributes:
    - name: email
      type: String!
    - name: firstName
      type: String!
    - name: lastName
      type: String!
  commands:
    ...
```

:::warning
Every `Type` declared has an hidden field `_id` that can be utilized by the Cache models
and the GraphQL Schema.
:::

## Refactoring the `sayHello` command

We will also refactor our `sayHello` command to use our new input `UserInput`. We need
to change both its declaration and its implementation.

```yml
# schema.yml

name: User
namespace: Byron

content:
  types:
    ...
  inputs:
  - name: UserInput
    attributes:
    - name: email
      type: String!
    - name: firstName
      type: String!
    - name: lastName
      type: String!
  commands:
    name: sayHello
    type: query
    returnType: String
    parameters:
    - name: data
      type: UserInput!
```

```typescript
// api/commands/sayHello.ts

const sayHello = (_p: any, args: any, _ctx: any, _i: any) => {
  return `Hello ${args.data.firstName} ${args.data.lastName} <${args.data.email}>`;
}

export default sayHello;
```

## Executing a query with and input

Build the component once more, but firstly we need to overwirte the previous build.

:::tip
Remember to terminate the component with `byron component down User`.
:::

```
byron component down User
byron component build User
```

Run the component by using the command:

```
byron component up User
```

After the component is up, go into the playground again, and refactor the query:

```graphql
query helloJohn {
  sayHello(data: {
    email: "john.doe@email.com"
    firstName: "John"
    lastName: "Doe"
  })
}
```

The response should be:

```json
{
  "data": {
    "sayHello": "Hello John Doe <john.doe@email.com>"
  }
}
```
