---
prev: ./mutations.md
---

# Handler
Handlers are event consumers, they execute their logic when the listened event
is broadcasted by the broker.

## Declaring a handler
The first step is to declare it into the `schema.yml`, a handler declaration takes two
arguments:

1. `name`: It will correspond to the name of the file inside `sink/handlers` that implements the 
functionality of this command.
2. `event`: The event that the handler will listen to.

```yml
# schema.yml

name: User
namespace: Byron

content:
  types:
    ...
  inputs:
    ...
  commands:
  - name: createUser
    type: mutation
    returnType: User!
    parameters:
    - name: data
      type: UserInput!
    ...
  handlers:
  - name: newUser
    event: 'new.user'
```

## Implementing a handler
The second step is to implement the handler. Every `handler` implementation takes two arguments:

1. `ctx`: This is global context of your Sink, in here you will find the cache connection.
2. `msg`: This the object broadcasted by the broker when an event is produced.

```typescript
// sink/handlers/newUser.ts

const newUser = async (ctx: any, msg: any) => {
  const data = JSON.parse(msg.getData());
  const user: any = await ctx.db.User.create({ ...data });
  console.log(user);
}

export default newUser;
```

This code will get the data from the message broadcasted by the broker, parse it into an
common object and save into the component's cache.

## Executing a handler
Build and re-run the component again:

```
byron component down User
byron component build User
byron component up User
```

Run the mutation we used in the last page, and check if the `user` object is logged
into console running the component.