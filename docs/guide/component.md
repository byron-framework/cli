---
prev: ./
next: ./command.md
---

# Component

Byron's main object is the **Byron Component**. To understand where it stands and what is
does, let's have a brief introduction to _Byron's Architecture_.

> we adopt the [C4 Model for Presenting Software Architectures](https://c4model.com)

## 1st C stands for Context

<img src="/images/c4model/Context.png" width="400" height="auto">

Byron is a framework to build general-purpose web backends -- there's no specialization in
Machine Learning or any other Artificial Inteligence technique, for example. A feature the
system must implement is the pattern [Event-Sourcing][1]. That is, the system must adopt a
message broker -- to persist the state of the application as a sequence of changes marked
as events in order. Such broker must provide an API so the services are able to subscribe
for updates and to publish new events.

## 2nd C stands for Containers

<img src="/images/c4model/Containers.png" width="400" height="auto">

We call **Byron Component** the main object built by the framework. It's inteded to implement
a coherent set of well defined actions, i.e., a [Bounded Context][2]. Byron Component adopts,
to external communication, a model different from the adopted to internal chatting. It exposes
an [GraphQL][3] API, an strongly typed interface alternative to [REST][4]. For the internal
communication, Byron Component exchanges [Domain Events][5] with the broker, publishing or
being notified accordingly to subscriptions.

## 3rd C stands for Components

> to avoid ambiguities, C4 "component" will be called as "C4 Component",
> while Byron "component" will be called as "Byron Component"

<img src="/images/c4model/Components.png" width="400" height="auto">

A Byron Component is divided into three microservices. First is the API, responsible for exposing
a GraphQL API and for handling HTTP requests. When a change in the application state is made, it
must then emit an Domain Event, instead of actively storing this data. When it needs any known data,
for validation purposes for instance, it queries the second microservice: the Cache. The building
block of the API are [commands](/guide/command).

The Cache is an in-memory database containing relevant data that came from the broker as events.
This storage provides decoupling between Byron Components because, in case of failure elsewhere, a
Byron Component has its own source data -- even if those data are slightly outdated.

The third microservice is the Sink, responsible for subscribing for all relevant events and, when
an event is notified, the Sink must update the Cache. The building block of the Sink are
[handlers](/guide/handler).

Those microservices implement the [Persistent Storage][6] pattern, providing more consistency among
copies of the same microservice -- either the API or the Sink. Besides that, the seggregation of
API and Sink arround the Cache implements [Command-Query Responsibility Seggregation][6], providing
more scalability.

## What we'll build

In this guide, we will build an application with two components, one to handle user creation, and
another to handle posts created by users. In this system, the user will be able to register,
create a post and list the posts from a user.

## Creating a component

As we mentioned earlier, we will create too components: the first to handle user creations,
and the other to handle users creating posts. But, first let's create a component to say "hello"
to us.

```bash
$ byron component init --name User --namespace Byron
```

This will generate a `User` directory. With this structure:

```
User/
|--> schema.yml
|--> api/
|    |--> commands/
|--> sink
|    |--> handlers/
|--> hooks/
```

The file `schema.yml` is a central file in a Byron's component, it contains all the declarations
to build the component. If we open it for now, it will have the declaration of component's `name`,
and its `namespace` -- the group of components that it belongs to.

```yml
# schema.yml

name: User
namespace: Byron
```

[1]: https://microservices.io/patterns/data/event-sourcing.html
[2]: https://martinfowler.com/bliki/BoundedContext.html
[3]: https://graphql.org/
[4]: https://en.wikipedia.org/wiki/Representational_state_transfer
[5]: https://microservices.io/patterns/data/domain-event.html
[6]: https://www.amazon.com/Microservices-Patterns-examples-Chris-Richardson/dp/1617294543
