#/usr/bin/env bash
_byron_complete()
{
  local cur prev

  COMPREPLY=()
  cur=${COMP_WORDS[COMP_CWORD]}
  prev=${COMP_WORDS[COMP_CWORD-1]}

  if [ $COMP_CWORD -eq 1 ]; then
    COMPREPLY=( $(compgen -W "component infrastructure" -- $cur) )
  elif [ $COMP_CWORD -eq 2 ]; then
    case "$prev" in
      "infrastructure")
        COMPREPLY=( $(compgen -W "up down" -- $cur) )
        ;;
      "component")
        COMPREPLY=( $(compgen -W "init build up down" -- $cur) )
        ;;
      *)
        ;;
    esac
  fi

  return 0
}

complete -F _byron_complete byron


