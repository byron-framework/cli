{{=__ __=}}
import Event from 'src/Event';
import __name__ from './dev__name__';
import { getPostHooks, getPreHooks } from '../hooks/factory';

export default (ctx: any) => {
  Event.listen('__event__', msg => {
    getPreHooks('__name__')(ctx, msg);
    __name__(ctx, msg);
    getPostHooks('__name__')(ctx, msg);
  });
};
__={{ }}=__
